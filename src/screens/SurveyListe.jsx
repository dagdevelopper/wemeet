import { Text, SafeAreaView, SectionList, Image, View, TouchableOpacity } from 'react-native'
import React, { useState, useEffect } from 'react'
import { styles } from '../style/styles';
import SurveyLineItem from '../components/SurveyLineItem';
import { EditIcon, PlusIcon } from '../../assets/icon';
import { useSelector } from 'react-redux';
import { MenuProvider } from 'react-native-popup-menu';
import { Pressable } from '@react-native-material/core';

const SurveyListe = ({ route, navigation }) => {
    const [DATA, setDatas] = useState([]);
    const [eventId, setEventId] = useState();
    const [role, setRole] = useState('');

    const { surveys } = useSelector(state => state.surveys);

    useEffect(() => {
        setDatas(surveys);
        setEventId(route.params.eventId);
        setRole(route.params.role);
    }, [route, surveys]);


    return (
        <SafeAreaView style={{ height: '98%', backgroundColor: 'white' }}>
            <MenuProvider>
                <SectionList
                    sections={DATA.map(s => {
                        return {
                            title: s.title,
                            description: s.description,
                            id: s.id,
                            data: s.surveyLines
                        }
                    })}
                    stickySectionHeadersEnabled={false}
                    keyExtractor={(item, index) => {
                        return item + index
                    }}
                    renderItem={({ item }) => {
                        return <SurveyLineItem data={item} role={role} />
                    }}
                    renderSectionHeader={({ section: { title, description, id } }) => (

                        <>

                            <View style={[{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginRight: 10 }]}>
                                <Text style={styles.surveyItem.title}> {'>'} {title}</Text>
                                {role === 'organizer' && <Pressable onPressOut={() => navigation.navigate('Gérer Sondage', { toUpdate: true, title: title, description: description, id })}>
                                    <Image source={EditIcon} />
                                </Pressable>}
                            </View>
                            {description && <View style={[styles.defaultBorder]}>
                                <Text style={[styles.defaultText, { color: 'black' }]}> {description} </Text>
                            </View>}
                        </>
                    )}

                    SectionSeparatorComponent={() => <View style={{ marginBottom: 13 }}></View>}
                    
                    renderSectionFooter={role !== 'organizer' ? undefined : ({ section: { data } }) => {

                        return (
                            <TouchableOpacity onPressOut={() => {
                                navigation.navigate('Gérer Suggestion', { survey: data[0].survey, toUpdate: false, label: '' });
                            }}>
                                <View style={[styles.button, styles.field]}>
                                    <Text style={[styles.defaultText, { color: 'black' }]}>Nouvelle suggestion</Text>
                                </View>
                            </TouchableOpacity>
                        )

                    }}

                    ListEmptyComponent={() => {
                        return (
                            <View style={styles.container}>
                                <Text style={[styles.defaultText, { color: 'black', fontSize: 30 }]}>Aucun Sondage !</Text>
                            </View>
                        )
                    }}
                />
            </MenuProvider>

            {role === 'organizer' && (
                <TouchableOpacity onPressOut={() => navigation.navigate('Gérer Sondage', { eventId, toUpdate: false, title: '', description: '' })}>
                    <Image source={PlusIcon} style={styles.surveyItem.plus} />
                </TouchableOpacity>
            )}

        </SafeAreaView>
    )
}


export default SurveyListe
