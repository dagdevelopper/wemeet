import { View, Text, TextInput, ScrollView, Pressable, Alert, TouchableOpacity, KeyboardAvoidingView, Image } from 'react-native'
import React, { useState } from 'react'
import Header from '../components/Header'
import { styles, defaultPadding, orangeClair, vert, noirCassé, rougeClair } from '../style/styles'
import FadeInView from '../components/FadeInView'
import { DoneIcon, ErrorIcon } from '../../assets/icon'
import HidePassword from '../components/HidePassword'
import { useUser } from '../ApiCalls/CustomHooks'
import PopupModal from '../components/PopupModal'

const Registration = ({ navigation }) => {
  const [lastName, setLastName] = useState('');
  const [firstName, setFirstName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmedPwd, setConfirmedPwd] = useState("");
  const [address, setAddress] = useState("");
  const [postalCode, setPostalCode] = useState("");

  const [errLastName, setErrLastName] = useState("");
  const [errFirstName, setErrFirstName] = useState("");
  const [errEmail, setErrEmail] = useState("");
  const [errPwd, setErrPwd] = useState("");
  const [errNewPwd, setErrNewPwd] = useState("");
  const [pwdEqual, setPwdEqual] = useState('none');
  const [secureText, setSecureText] = useState(true);
  const [secureTextNewPwd, setSecureTextNewPwd] = useState(true);
  const [show, setShow] = useState(false);
  const [showError, setShowError] = useState(false);
  const [error, setError] = useState('');

  const { addUser } = useUser();

  const containsError = () => {
    let hasError = false;

    if (lastName.trim() === '') {
      setErrLastName('Votre nom est obligatoire !');
      hasError = true;
    } else setErrLastName('');

    if (firstName.trim() === '') {
      setErrFirstName('Votre prénom est obligatoire !');
      hasError = true;
    } else setErrFirstName('');

    if (email.trim() === '') {
      setErrEmail("L' e-mail est obligatoire !");
      hasError = true;
    } else setErrEmail('');

    if (password.trim() === '') {
      setErrPwd('Le mot de passe est obligatoire !');
      hasError = true;
    } else setErrPwd('');

    if (confirmedPwd.trim() === '') {
      setErrNewPwd('confirmer le mot de passe !');
      setPwdEqual('bad');
      hasError = true;
    } else {
      if (pwdEqual === 'good') {

        setErrNewPwd('');
      } else {
        setPwdEqual('bad');
        setErrNewPwd('Les mots de passe sont différents !');
        hasError = true;
      }
    }

    return hasError;

  }

  const handleClick = () => {
    navigation.navigate("Log-in");
  }
  const handleRegistration = () => {
    if (!containsError()) {
      addUser({ email, lastName, firstName, postalCode, address, isAdmin: false, password }).then(() => {
        setShow(true);
        setEmail('');
        setFirstName('');
        setLastName('');
        setPassword('');
        setAddress('');
        setConfirmedPwd('');
        setPostalCode('');
        setSecureText(true);

        initErrMsg();

      }).catch(e => {
        setShowError(true);
       
        if (e.code){
          if (e.code === 'ERR_NETWORK'){
            setError('Erreur de connexion au serveur !');
          }else if (e.code === "ERR_BAD_REQUEST"){
            setError("Compte déja présent, ou mlauvais format du mot de passe et/ou de l'email !");
            
          }
        }else{
          setError('Vous êtes déja inscrit !');
        }
      })

    }
  }

  const initErrMsg = () => {
    setErrEmail('');
    setErrFirstName('');
    setErrLastName('');
    setErrNewPwd('');
    setErrPwd('');
    setErrNewPwd('');
    setPwdEqual('none');
  }


  const handleCheckPwd = () => {
    if (confirmedPwd !== '') {
      
      if (password === confirmedPwd) {
        setPwdEqual('good');
        setErrNewPwd('');
      } else {
        setPwdEqual('bad');
        setErrNewPwd('Les mots de passe sont différents !');
      }
    } else {
      setPwdEqual('none');
      setErrNewPwd('');
    }
  }

  return (
    <FadeInView duration={1000}>
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <ScrollView nestedScrollEnabled={true} showsVerticalScrollIndicator={false}>
          <View style={[styles.container]}>
            <View style={{ justifyContent: 'flex-start' }}><View><Header title={'Registration'} /></View></View>
            <View style={styles.field}>

              <TextInput
                style={styles.input}
                value={firstName}
                onChangeText={(f) => setFirstName(f)}
                placeholder={'Prénom*'}
                textContentType='name'
              />
            </View>
            {errFirstName && <Text style={[styles.error, { padding: 2 }]}> {errFirstName} </Text>}

            <View style={styles.field}>

              <TextInput
                style={styles.input}
                value={lastName}
                onChangeText={(l) => setLastName(l)}
                placeholder={'Nom*'}
                textContentType='familyName'
              />
            </View>
            {errLastName && <Text style={[styles.error, { padding: 2 }]}> {errLastName} </Text>}

            <View style={styles.field}>

              <TextInput
                style={styles.input}
                value={email}
                onChangeText={(e) => setEmail(e)}
                placeholder={'Adresse E-mail*'}
                textContentType='emailAddress'
              />
            </View>
            {errEmail && <Text style={[styles.error, { padding: 2 }]}> {errEmail} </Text>}

            <PopupModal
              message={error}
              icon={ErrorIcon}
              bgColor={rougeClair}
              colorIcon={'black'}
              colorMsg={noirCassé}
              show={showError}
              setShow={setShowError}
            />
            <PopupModal
              message={'inscription réussie !'}
              icon={DoneIcon}
              bgColor={orangeClair}
              colorIcon={vert}
              colorMsg={noirCassé}
              show={show}
              setShow={setShow}
            />
            
            <View style={styles.field}>

              <TextInput
                style={styles.input}
                value={password}
                onChangeText={(p) => setPassword(p)}
                placeholder={'Mot de passe*'}
                textContentType="newPassword"
                secureTextEntry={secureText}
              />
              <HidePassword secureText={secureText} setSecureText={setSecureText} />
              {pwdEqual === 'good' && <Image source={DoneIcon} style={[styles.icon, { tintColor: 'green', right: 5, position: 'absolute', top: 11 }]} />}
            </View>
            {errPwd && <Text style={[styles.error, { padding: 2 }]}> {errPwd} </Text>}

            <View style={styles.field}>

              <TextInput
                style={[styles.input, (pwdEqual === 'bad' ? styles.password.bad : undefined)]}
                value={confirmedPwd}
                onChangeText={(c) => setConfirmedPwd(c)}
                placeholder={'Confirmer le mot de passe*'}
                secureTextEntry={secureTextNewPwd}
                onEndEditing={() => handleCheckPwd()}
                
              />
              <HidePassword secureText={secureTextNewPwd} setSecureText={setSecureTextNewPwd} />
              {pwdEqual === 'good' && <FadeInView duration={500}><Image source={DoneIcon} style={[styles.icon, { tintColor: 'green', left: 2, position: 'absolute', top: 5 }]} /></FadeInView>}
            </View>
            {errNewPwd && <Text style={[styles.error, { padding: 2 }]}> {errNewPwd} </Text>}

            <View style={styles.field}>

              <TextInput
                style={styles.input}
                value={address}
                onChangeText={(a) => setAddress(a)}
                placeholder={'Rue et numéro'}
                textContentType="streetAddressLine1"
              
              />
            </View>

            <View style={styles.field}>

              <TextInput
                style={styles.input}
                value={postalCode}
                onChangeText={(c) => setPostalCode(c)}
                placeholder={'Code postal'}
                textContentType="postalCode"
                keyboardType='numeric'
              />
            </View>
            <Text style={[styles.text, { padding: defaultPadding, color: 'black', fontWeight: '200', fontStyle: 'italic' }]}>* champs obligatoires</Text>

            <TouchableOpacity onPress={handleRegistration}><View style={styles.button}><Text style={styles.text}>S'inscrire</Text></View></TouchableOpacity>
            <Pressable onPress={() => handleClick()}>
              <Text style={styles.link}>Besoin d'un compte ? Se connecter ! </Text>
            </Pressable>


          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </FadeInView>
  )
}

export default Registration