import React, { useContext, useEffect, useState } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { useDispatch, useSelector } from 'react-redux';
import EventListe from '../components/EventListe';
import TabBar from '../components/TabBar';
import { setEvents, setMyEvents, updateListEvents } from '../redux_wemeet/Slices/event.slice';
import { noirCassé, rougeClair, styles } from '../style/styles';
import { useEvent, useEventRole } from '../ApiCalls/CustomHooks';
import PopupModal from '../components/PopupModal';
import { ErrorIcon } from '../../assets/icon';
import authProvider from '../auth/authProvider';
import EventInvite from '../components/EventInvite';


const Home = ({ navigation }) => {

    const [searchCity, setSearchCity] = useState("");
    const [clicked, setClicked] = useState(false);
    const { events, eventsToShow } = useSelector((state) => state.events);
    const dispatch = useDispatch();
    const { getAllEvent } = useEvent();
    const [showError, setShowError] = useState(false);
    const [msgError, setErrorMsg] = useState('');
    const { getMyEvents } = useEventRole();
    const { userConnected } = useContext(authProvider);
    const [datas, setDatas] = useState(events);

    useEffect(() => {

        if (searchCity === "") {
            getAllEvent(userConnected).then(data => {
                dispatch(setEvents(data));
                setDatas(data.filter((e => e.is_private === false)))
            }).catch(e => {
                console.log("erreur get all event : ", e)
                setShowError(!showError);
                setErrorMsg(`Impossible de charger les wemeets !`);
            })
        } else {
            dispatch(updateListEvents(searchCity));
            setDatas(eventsToShow?.filter((e => e.is_private === false)));
        }

        getMyEvents(userConnected.id).then(data => {

            const { participated, organized } = data.data;
            dispatch(setMyEvents({ organized: organized, participated: participated }))

        }).catch(e => console.log(e));

    }, [searchCity]);


    useEffect(() => {
        setDatas(events);
    }, [events]);

    const separator = () => {
        return <View style={styles.horizontalLine}></View>;
    }


    const listEmpty = () => {
        return (
            datas ? (
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 42 }}>
                    <Text style={[styles.title]}>Oups !</Text>
                    <Text style={[styles.defaultText, { color: 'black', alignSelf: 'center', margin: 30 }]}>
                        Dommage, aucun évènement ne correspond à votre recherche !
                    </Text>
                </View>
            ) : (
                <ActivityIndicator style={styles.container} color={'orange'} accessibilityLabel='Chargement' size={'large'} />
            )

        )
    }


    return (
        <SafeAreaView onLayout={() => {

            getAllEvent(userConnected).then(datas => {
                dispatch(setEvents(datas));
                setDatas(datas.filter((e => e.is_private === false)));
            }).catch(e => {
                console.log("erreur get all event : ", e)
                setShowError(!showError);
                setErrorMsg(`Impossible de charger les wemeets !`);
            })
        }} style={[styles.home.container]}>

            <TabBar
                navigation={navigation}
                title={'Evènements'}
                clicked={clicked}
                setClicked={setClicked}
                setSearchCity={setSearchCity}
                searchCity={searchCity}
            />
            <PopupModal
                message={msgError}
                bgColor={rougeClair}
                icon={ErrorIcon}
                colorIcon={noirCassé}
                show={showError}
                setShow={setShowError}
            />
            <View style={styles.container}>
            <EventInvite event={datas[0]} accepted={false} />
                <EventListe
                    emptyList_fct={listEmpty}
                    events={datas}
                    setRefreshControl={true}
                    onRefreshAction={() => {
                        getAllEvent(userConnected).then(datas => {
                            dispatch(setEvents(datas));
                            setDatas(datas.filter((e => e.is_private === false)));
                            getMyEvents(userConnected.id).then(data => {

                                const { participated, organized } = data.data;
                                dispatch(setMyEvents({ organized: organized, participated: participated }));

                            }).catch(e => {
                                console.log("erreur get all event : ", e)
                                setShowError(!showError);
                                setErrorMsg(`Impossible de charger les wemeets !`);
                            });
                        }).catch(e => {

                            console.log('error', e.message);
                            setShowError(!showError);
                            setErrorMsg(`${e.message}`);
                        })
                    }}
                    separatorComponent={() => separator()}
                />
            </View>
        </SafeAreaView>
    );
};

export default Home;