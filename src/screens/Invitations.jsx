import { View, Text, SectionList, ActivityIndicator } from 'react-native'
import React from 'react'
import { primaryColor, styles } from '../style/styles'
import { useEffect } from 'react';
import { useState } from 'react';
import { FlatList } from 'react-native';
import EventInvite from '../components/EventInvite';

const Invitations = ({ route }) => {
    const [invitations, setInvitations] = useState([]);

    useEffect(() => {
        setInvitations(route.params.invitations);

    }, [route]);

    const separator = () => {
        return <View style={styles.borderSeparator}></View>;
    }

//event ne donne pas
    return (
        <View style={styles.container}>
            
            <EventInvite event={invitations[0]?.invitations[0]?.event} accepted={false} />
            <SectionList
            
                sections={invitations.map(s => {
                    return {
                        title: `${s.sender.first_name} ${s.sender.last_name}`,
                        data: s.invitations
                    }
                })}

                renderItem={({ item, section }) => {

                    return <FlatList
                        horizontal
                        data={section.data}
                        renderItem={() => <EventInvite event={item.event} accepted={item.accepted} />}
                        ItemSeparatorComponent={separator}
                    />
                }}
                renderSectionHeader={({ section: { title } }) => (
                    <View style={[{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between', marginRight: 10, width: 400 }, styles.defaultBorder]}>
                        <Text style={[styles.defaultText, { color: 'black' }]}> {' >'} Invitations de :</Text>
                        <Text style={[styles.surveyItem.title, { fontStyle: 'italic', fontSize: 23 }]}> {title}</Text>
                    </View>
                )}

                SectionSeparatorComponent={() => <View style={{ marginBottom: 13 }}></View>}
                ListEmptyComponent={() => {
                    return (
                        <View style={styles.container}>
                            {invitations ? <Text style={[styles.defaultText, { color: 'black', fontSize: 30 }]}>Aucune invitation !</Text> : <ActivityIndicator size={'large'} color={primaryColor} />}
                        </View>
                    )
                }}
            />
        </View>
    )
}

export default Invitations