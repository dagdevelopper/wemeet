import { View, Text } from "react-native";
import React from "react";
//import { useSelector } from "react-redux";
import { useInvitation, useUser } from "../ApiCalls/CustomHooks";
import { useState } from "react";
import SearchBarApp from "../components/SearchBarApp";
import { styles } from "../style/styles";
import { useEffect } from "react";
import UserToInviteItem from "../components/UserToInviteItem";
import { useContext } from "react";
import authProvider from "../auth/authProvider";

const InvitationScreen = ({ route }) => {
  const { participants, event } = route.params;
  const [invitations, setInvitations] = useState([]);
  const { getInvitationsByEventId } = useInvitation();

  const { getUsers } = useUser();
  const [keyword, setKeyword] = useState("");
  const [usersToShow, setUsersToShow] = useState([]);
  const [users, setUsers] = useState([]);
  const { userConnected } = useContext(authProvider);

  useEffect(() => {
    getUsers()
      .then((datas) => {
        setUsers(datas);

        getInvitationsByEventId({ eventId: event?.id })
          .then((res) => {
            setInvitations(res.data.invitations);
          })
          .catch((e) => console.log("get invite by event error : ", e));
      })
      .catch((e) => console.log("getUser invite screen error : ", e));
  }, [event]);

  useEffect(() => {
    handleUserToShow(keyword);
  }, [keyword]);

  const handleUserToShow = (keyword) => {
    if (keyword.length > 0) {
      
      const result = users.filter(
        (user) =>
          user.email !== userConnected.email &&
          (user?.email.includes(keyword) ||
          user?.first_name.includes(keyword) ||
          user?.last_name.includes(keyword))
      );
      
      setUsersToShow(result);
    }else{
      setUsersToShow([]);
    }
  };

  const invitationAlreadyAccepted = ({ guest }) => {
    const participated =
      participants?.find((p) => p?.email === guest) !== undefined;

    let invited = false;
    if (Array.isArray(invitations) && invitations.length > 0) {
      for (const i of invitations) {
        if (
          i?.sender === userConnected?.email &&
          i?.guest === guest &&
          i?.accepted === true
        ) {
          invited = true;
          break;
        }
      }
    } else invited = false;

    return participated || invited;
  };

  return (
    <View style={styles.invitations.container}>
      <View style={styles.invitations.searchBlock}>
        <SearchBarApp searchCity={keyword} setSearchCity={setKeyword} />
      </View>
      <View>
        {usersToShow?.map((user, key) => {
          const inviteAccepted = invitationAlreadyAccepted({
            guest: user?.email,
          });
         
          return (
            <UserToInviteItem
              key={key}
              user={user}
              alreadyInvited={inviteAccepted}
              invitations={invitations}
              eventId={event?.id}
            />
          );
        })}
      </View>
    </View>
  );
};

export default InvitationScreen;
