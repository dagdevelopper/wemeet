import { Text, TextInput, KeyboardAvoidingView, TouchableOpacity } from 'react-native'
import React, { useState, useEffect } from 'react'
import { noirCassé, orangeClair, rougeClair, styles, vert } from '../style/styles'
import { useSurveyLine } from '../ApiCalls/CustomHooks';
import { useDispatch } from 'react-redux';
import { addSurveyLineState, updateSurveyLineState } from '../redux_wemeet/Slices/survey.slice';
import PopupModal from '../components/PopupModal';
import { DoneIcon, ErrorIcon } from '../../assets/icon';

const NewSurveyLine = ({ route, navigation }) => {
  const [survey, setSurvey] = useState();
  const [line_number, setLineNumber] = useState();
  const [label, setLabel] = useState('');
  const [nb_votes, setNbVotes] = useState(0);
  const [liked, setLiked] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  const [showError, setShowError] = useState(false);
  const [error, setError] = useState('');
  const [show, setShow] = useState(false);
  const [msgModal, setMsgModal] = useState('');
  const { createSurveyLine, updateSurveyLine } = useSurveyLine();
  const dispatch = useDispatch();


  useEffect(() => {
    setSurvey(route.params.survey);
    setLabel(route.params.label);
    setLineNumber(route.params.line_number);
    setNbVotes(route.params.nb_votes);
    setLiked(route.params.liked);
  }, [route]);

  const addSurveyLine = () => {
    if (validForm()) {
      createSurveyLine({ label, survey }).then(res => {

        dispatch(addSurveyLineState(res.data));
        setLabel('');

        setShow(!show);
        setMsgModal('Suggestion crée avec succès ! ');
      }).catch(e => {
        setShowError(!showError);
        setError(`Erreur lors de l'ajout de la suggestion : ${e.message}`);
        console.error("error add sl : ", e);
      });

    }
  }

  const updateSl = () => {
    if (validForm()) {
      updateSurveyLine({ label, survey, line_number, nb_votes, liked }).then(res => {

        dispatch(updateSurveyLineState(res.data));
        navigation.goBack();
      }).catch(e => {
        setShowError(!showError);
        setError(`Erreur lors de la mise à jour de la suggestion : ${e.message}`);
        console.error("error update sl : ", e)
      });

    }
  }

  const validForm = () => {
    let valid = true;
    if (label === '') {
      valid = false;
      setErrorMsg('Ce champ est obligatoire !');
    }

    return valid;
  }



  return (
    <KeyboardAvoidingView behavior='padding' style={{ alignItems: 'center', backgroundColor: 'white', height: '100%' }}>
      <TextInput onFocus={() => setErrorMsg('')} style={[styles.input, styles.defaultText, { color: noirCassé }]} value={label} placeholder={'Libellé'} placeholderTextColor={noirCassé} onChangeText={(l) => setLabel(l)} />
      {errorMsg !== '' && <Text style={styles.error}> {errorMsg} </Text>}
      <TouchableOpacity style={[styles.button, { width: '85%' }]} onPressOut={route.params.toUpdate ? updateSl : addSurveyLine}>
        <Text style={[styles.subTitle, { color: noirCassé }]}> {route.params.toUpdate ? 'Modifier' : 'Ajouter'} </Text>
      </TouchableOpacity>

      <PopupModal
        message={msgModal}
        bgColor={orangeClair}
        icon={DoneIcon}
        colorIcon={vert}
        show={show}
        setShow={setShow}
      />
      <PopupModal
        message={error}
        bgColor={rougeClair}
        icon={ErrorIcon}
        colorIcon={noirCassé}
        show={showError}
        setShow={setShowError}
      />

    </KeyboardAvoidingView>
  )
}

export default NewSurveyLine