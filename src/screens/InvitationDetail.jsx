import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import React from 'react';
import { noirCassé, primaryColor, styles } from '../style/styles';
import { useEffect } from 'react';
import { useState } from 'react';
import { EventIcon, EventUnknown, LocationIcon, PersonIcon, PrivateIcon, PublicIcon } from '../../assets/icon';
import { dateToString } from '../util/utils';

const InvitationDetail = ({ route, navigation }) => {
    const [event, setEvent] = useState();

    useEffect(() => {
        setEvent(route.params.event);

    }, [route]);


    return (
        <ScrollView contentContainerStyle={[styles.invitation.container]}>

            <View style={styles.invitation.bloc}>
                <Text style={styles.surveyItem.title}>Titre</Text>
                <View style={styles.invitation.blockWithImage}>
                    <Image source={EventUnknown} style={styles.icon} />
                    <Text style={[styles.defaultText, { color: 'black' }]}> {event?.label} </Text>
                </View>
            </View>

            <View style={styles.invitation.bloc}>
                <Text style={styles.surveyItem.title}>Location</Text>
                <View style={styles.invitation.blockWithImage}>
                    <Image source={LocationIcon} style={styles.icon} />
                    <Text style={[styles.defaultText, { color: 'black' }]}> {event?.city_name} </Text>
                </View>
            </View>

            <View style={styles.invitation.bloc}>
                <Text style={styles.surveyItem.title}>Date</Text>
                <View style={styles.invitation.blockWithImage}>
                    <Image source={EventIcon} style={styles.icon} />
                    <Text style={[styles.defaultText, { color: 'black' }]}>
                        {` ${dateToString(new Date(event?.date_begin))}  ${event?.date_begin !== event?.date_end ? ` - ${dateToString(new Date(event?.date_end))}` : ''}`}
                    </Text>
                </View>
            </View>

            <View style={styles.invitation.bloc}>
                <Text style={styles.surveyItem.title}>Nombre de participants</Text>
                <TouchableOpacity onPress={() => {
                    navigation.navigate('ParticipantsListe', {
                        participants: event?.participants,
                        label: event?.label
                    });
                }} style={[styles.invitation.blockWithImage, {justifyContent : 'space-between', alignItems : 'center'}]}>
                    <View  style={[styles.invitation.blockWithImage]}>
                        <Image source={PersonIcon} style={styles.icon} />
                        <Text style={[styles.defaultText, { color: 'black' }]}> {event?.participants?.length} participant{event?.participants?.length > 1 ? `s` : ``} </Text>
                    </View>
                    <Text style={[styles.title, {color : primaryColor}]}> {'>'} </Text>
                </TouchableOpacity>
            </View>

            <View style={styles.invitation.bloc}>
                <Text style={styles.surveyItem.title}>Visibilité</Text>
                <View style={styles.invitation.blockWithImage}>
                    <Image source={event?.is_private ? PrivateIcon : PublicIcon} style={styles.icon} />
                    <Text style={[styles.defaultText, { color: 'black' }]}> {event?.is_private ? 'Privée' : 'Public'} </Text>
                </View>
            </View>

            <View style={styles.invitation.bloc}>
                <Text style={styles.surveyItem.title}>Description</Text>
                <View style={styles.invitation.blockWithImage}>
                    <Text style={[styles.subTitle, { color: 'darkgray' }]}> {event?.description} </Text>
                </View>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: '100%', padding: 12, marginTop: 57 }}>
                <TouchableOpacity onPress={() => navigation.goBack()} style={[styles.defaultRoundedBorder, { backgroundColor: noirCassé,  width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 37 }]}>
                    <Text style={[styles.text, { color: 'whitesmoke', padding: 7 }]}> Retour </Text>
                </TouchableOpacity>

            </View>

        </ScrollView>
    );
}

export default InvitationDetail;