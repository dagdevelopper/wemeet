import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  KeyboardAvoidingView,
  ActivityIndicator,
  Alert,
} from "react-native";
import React, { useEffect, useState } from "react";
import {
  blue,
  noirCassé,
  orangeClair,
  primaryColor,
  rougeClair,
  styles,
  vert,
} from "../style/styles";
import {
  CloseIcon,
  DoneIcon,
  EditIcon,
  ErrorIcon,
  EventUnknown,
  InvitIcon,
  LogoutIcon,
  NextIcon,
  ProfileIcon,
  VerifiedBadgeIcon,
  WemeetIcon,
} from "../../assets/icon";
import { useInvitation, useUser } from "../ApiCalls/CustomHooks";
import { hasAuthentication, logout } from "../auth/authentication";
import { useContext } from "react";
import Auth from "../auth/authProvider";
import ModalContainer from "../components/ModalContainer";
import NewEvent from "./NewEvent";
import PopupModal from "../components/PopupModal";
import Badge from "../components/Badge";
import { useIsFocused } from "@react-navigation/native";
import InvitationStack from "../navigation/InvitationStack";
import { useDispatch, useSelector } from "react-redux";
import { resetStateEvent } from "../redux_wemeet/Slices/event.slice";
import {
  resetStateInvite,
  setinvitations,
} from "../redux_wemeet/Slices/invitation.slice";
import { resetStateSurvey } from "../redux_wemeet/Slices/survey.slice";

const defaultPadding = 6;

const Profile = ({ navigation }) => {
  const [toEdit, setToEdit] = useState(false);
  const [lastName, setLastName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAdress] = useState("");
  const [postalCode, setPostalCode] = useState();
  const [userId, setUserId] = useState();
  const [bgLoading, setBgLoading] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [openModalInv, setOpenModalInv] = useState(false);
  const [errLastName, setErrLastName] = useState("");
  const [errFirstName, setErrFirstName] = useState("");
  const [errEmail, setErrEmail] = useState("");
  const [showError, setShowError] = useState(false);
  const [error, setError] = useState("");
  const [show, setShow] = useState(false);
  const [msgModal, setMsgModal] = useState("");

  const {
    setIsAuthenticated,
    setUserConnected,
    nbEventsOrganized,
    nbEventsParticipated,
    userConnected,
  } = useContext(Auth);
  const { updateUser } = useUser();
  const [user, setUserState] = useState(userConnected);
  const { nbUnread } = useSelector((state) => state.invitations);
  const dispatch = useDispatch();
  const [invitations, setInvitations] = useState();
  const [nbUnreadInvite, setnbUnreadInvite] = useState(0);
  const focus = useIsFocused();
  const { getInvitations } = useInvitation();

  

  useEffect(() => {
    if (focus) {
      hasAuthentication()
        .then((res) => {
          if (res.isAuthneticated) {
            setUserId(res.userDatas.id);
            setUserState(userConnected);

            getInvitations({ email: res.userDatas.email })
              .then((res) => {
                
                setInvitations(res.data?.invites);
                dispatch(setinvitations(res.data));
                setnbUnreadInvite(nbUnread);
              })
              .catch((e) =>
                console.log("error get invitations (profile) : ", e)
              );
          } else {
            Alert.alert(
              "Token expiré",
              "Votre connexion est expiré ! veuillez vous reconnecter",
              [
                {
                  text: "Se connecter",
                  onPress: () => {
                    setIsAuthenticated(false);
                    navigation.replace("Login");
                  },
                },
              ]
            );
          }
        })
        .catch((e) => {
          setIsAuthenticated(false);
          navigation.replace("Login");
          console.log("Error auth: ", e);
        });
    }

    setAll();
  }, [userConnected, nbUnread, focus]);

  const containsError = () => {
    let hasError = false;

    if (lastName?.trim() === "") {
      setErrLastName("Votre nom est obligatoire !");
      hasError = true;
    } else setErrLastName("");

    if (firstName?.trim() === "") {
      setErrFirstName("Votre prénom est obligatoire !");
      hasError = true;
    } else setErrFirstName("");

    if (email?.trim() === "") {
      setErrEmail("L' e-mail est obligatoire !");
      hasError = true;
    } else setErrEmail("");

    return hasError;
  };

  const handleUpdateUser = () => {
    if (
      !containsError() &&
      (lastName !== user.lastName ||
        firstName !== user.firstName ||
        email !== user.email ||
        postalCode !== user.postalCode ||
        address !== user.address)
    ) {
      updateUser({
        firstName,
        lastName,
        email,
        address,
        postalCode,
        id: user.id,
        isAdmin: user.is_admin,
      })
        .then((res) => {
          setUserConnected(res.data);
          setToEdit(false);
          setShow(!show);
          setMsgModal("Données personnelles mise à jour ! ");
        })
        .catch((e) => {
          setShowError(!showError);
          setError(
            `Erreur lors de la mise à jour des informations personnelles !`
          );
          console.error("error update : ", e);
        });
    }
  };

  const handleLogout = () => {
    Alert.alert("Confirmer", "Souhaitez-vous vous déconnecter ? ", [
      {
        text: "Annuler",
        style: "cancel",
      },
      {
        text: "Confirmer",
        onPress: () => {
          logout()
            .then(() => {
              setIsAuthenticated(false);
              dispatch(resetStateEvent());
              dispatch(resetStateInvite());
              dispatch(resetStateSurvey());
              navigation.replace("Login");
            })
            .catch((e) => {
              navigation.replace("Login");
              console.log("error disconnect", e);
            });
        },
      },
    ]);
  };

  const setAll = () => {
    setEmail(user?.email);
    setFirstName(user?.first_name);
    setLastName(user?.last_name);
    setAdress(user?.address);
    setPostalCode(user?.postal_code);
  };

  return (
    <KeyboardAvoidingView behavior="padding">
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ backgroundColor: "white" }}
      >
        <View style={[styles.profile.container]}>
          <ImageBackground
            onLoadStart={() => setBgLoading(!bgLoading)}
            onLoad={() => {
              setBgLoading(!bgLoading);
              setAll();
            }}
            progressiveRenderingEnabled
            source={WemeetIcon}
            blurRadius={16}
            fadeDuration={1000}
            style={styles.profile.header.container}
          >
            <Image style={styles.profile.header.image} source={ProfileIcon} />
            <View style={styles.field}>
              <Text style={styles.profile.header.name}>
                {" "}
                {user?.first_name} {user?.last_name}{" "}
              </Text>
              {user.is_admin && (
                <Image
                  source={VerifiedBadgeIcon}
                  style={[
                    styles.icon,
                    { tintColor: blue, width: 19, height: 19 },
                  ]}
                />
              )}
            </View>
            <Text style={[styles.defaultText, styles.shadow]}>
              {" "}
              {user?.email}{" "}
            </Text>
            {bgLoading && (
              <ActivityIndicator color={"black"} style={{ marginTop: 9 }} />
            )}

            <View style={styles.profile.recap.container}>
              <View style={styles.profile.recap.item}>
                <Text style={styles.profile.recap.item.number}>
                  {" "}
                  {userId ? nbEventsOrganized : <ActivityIndicator />}{" "}
                </Text>
                <Text style={[styles.defaultText, styles.shadow]}>
                  Events organisés
                </Text>
              </View>

              <View style={styles.profile.recap.item}>
                <Text style={styles.profile.recap.item.number}>
                  {" "}
                  {userId ? nbEventsParticipated : <ActivityIndicator />}{" "}
                </Text>
                <Text style={[styles.defaultText, styles.shadow]}>
                  Participations
                </Text>
              </View>
            </View>
          </ImageBackground>

          <View style={styles.profile.body.container}>
            <TouchableOpacity
              onPress={() => setOpenModal(true)}
              style={styles.profile.body.onglet}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Image
                  style={{ width: 40, height: 40, tintColor: "orange" }}
                  source={EventUnknown}
                />
                <View style={{ marginLeft: 12 }}>
                  <Text style={styles.subTitle}>Créer un Wemeet</Text>
                  <Text style={[styles.defaultText, { color: "black" }]}>
                    Organisez votre propre évènement
                  </Text>
                </View>
              </View>
              <Image source={NextIcon} style={{ width: 25, height: 25 }} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                setOpenModalInv(!openModalInv);
              }}
              style={styles.profile.body.onglet}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Image
                  style={{ width: 40, height: 40, tintColor: "orange" }}
                  source={InvitIcon}
                />
                <View style={{ marginLeft: 12 }}>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={styles.subTitle}>Vos invitations</Text>
                    {nbUnreadInvite !== 0 && <Badge number={nbUnreadInvite} />}
                  </View>
                  <Text style={[styles.defaultText, { color: "black" }]}>
                    Consultez vos invitations
                  </Text>
                </View>
              </View>
              <Image source={NextIcon} style={{ width: 25, height: 25 }} />
            </TouchableOpacity>

            {/*  INFORMATIONS PERSONNELLES */}

            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                marginRight: 12,
              }}
            >
              <Text style={styles.title}>Informations personnelles</Text>
              <TouchableOpacity
                onPress={() => {
                  if (toEdit) {
                    setAll();
                  }
                  setToEdit(!toEdit);
                }}
              >
                {toEdit ? (
                  <Image
                    style={{ width: 31, height: 31, tintColor: primaryColor }}
                    source={CloseIcon}
                  />
                ) : (
                  <Image style={{ width: 21, height: 21 }} source={EditIcon} />
                )}
              </TouchableOpacity>
            </View>

            <View style={styles.profile.detail.container}>
              <View style={styles.profile.detail.item}>
                <Text
                  style={[
                    styles.defaultText,
                    { color: "black", paddingBottom: defaultPadding },
                  ]}
                >
                  Prénom
                </Text>
                {!toEdit ? (
                  <Text style={styles.text}> {user?.first_name} </Text>
                ) : (
                  <>
                    <TextInput
                      textContentType="name"
                      onChangeText={(e) => setFirstName(e)}
                      style={styles.input}
                      defaultValue={user.first_name}
                    />
                    {errFirstName && (
                      <Text style={[styles.error, { padding: 2 }]}>
                        {" "}
                        {errFirstName}{" "}
                      </Text>
                    )}
                  </>
                )}
              </View>

              <View style={styles.profile.detail.item}>
                <Text
                  style={[
                    styles.defaultText,
                    { color: "black", paddingBottom: defaultPadding },
                  ]}
                >
                  Nom
                </Text>
                {!toEdit ? (
                  <Text style={styles.text}> {user?.last_name} </Text>
                ) : (
                  <>
                    <TextInput
                      textContentType="familyName"
                      onChangeText={(e) => setLastName(e)}
                      style={styles.input}
                      defaultValue={user.last_name}
                    />
                    {errLastName && (
                      <Text style={[styles.error, { padding: 2 }]}>
                        {" "}
                        {errLastName}{" "}
                      </Text>
                    )}
                  </>
                )}
              </View>

              <View style={styles.profile.detail.item}>
                <Text
                  style={[
                    styles.defaultText,
                    { color: "black", paddingBottom: defaultPadding },
                  ]}
                >
                  E-mail
                </Text>
                {!toEdit ? (
                  <Text style={styles.text}> {user?.email} </Text>
                ) : (
                  <>
                    <TextInput
                      textContentType="emailAddress"
                      onChangeText={(e) => setEmail(e)}
                      style={styles.input}
                      defaultValue={user.email}
                    />
                    {errEmail && (
                      <Text style={[styles.error, { padding: 2 }]}>
                        {" "}
                        {errEmail}{" "}
                      </Text>
                    )}
                  </>
                )}
              </View>

              <View style={styles.profile.detail.item}>
                <Text
                  style={[
                    styles.defaultText,
                    { color: "black", paddingBottom: defaultPadding },
                  ]}
                >
                  Adresse
                </Text>
                {!toEdit ? (
                  <Text style={styles.text}> {user?.address} </Text>
                ) : (
                  <TextInput
                    textContentType="streetAddressLine1"
                    onChangeText={(e) => setAdress(e)}
                    style={styles.input}
                    defaultValue={user.address}
                  />
                )}
              </View>

              <View style={styles.profile.detail.item}>
                <Text
                  style={[
                    styles.defaultText,
                    { color: "black", paddingBottom: defaultPadding },
                  ]}
                >
                  Code postal
                </Text>
                {!toEdit ? (
                  <Text style={styles.text}> {user?.postal_code} </Text>
                ) : (
                  <TextInput
                    keyboardType="numeric"
                    onChangeText={(e) => setPostalCode(e)}
                    style={styles.input}
                    defaultValue={`${user.postal_code}`}
                  />
                )}
              </View>

              {toEdit && (
                <TouchableOpacity
                  onPress={handleUpdateUser}
                  style={styles.button}
                >
                  <Text style={styles.text}>Enregistrer</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>

          <TouchableOpacity onPress={() => handleLogout()}>
            <View style={[styles.button, styles.profile.body.deconnexion]}>
              <Image
                source={LogoutIcon}
                style={[styles.icon, { tintColor: "black", marginRight: 4 }]}
              />
              <Text style={styles.text}>Déconnexion</Text>
            </View>
          </TouchableOpacity>

          <PopupModal
            message={msgModal}
            bgColor={orangeClair}
            icon={DoneIcon}
            colorIcon={vert}
            show={show}
            setShow={setShow}
          />
          <PopupModal
            message={error}
            bgColor={rougeClair}
            icon={ErrorIcon}
            colorIcon={noirCassé}
            show={showError}
            setShow={setShowError}
          />
        </View>
      </ScrollView>

      <ModalContainer show={openModal} setShow={setOpenModal}>
        <NewEvent navigation={navigation} />
      </ModalContainer>

      <ModalContainer show={openModalInv} setShow={setOpenModalInv}>
        <InvitationStack invitations={invitations} />
      </ModalContainer>
    </KeyboardAvoidingView>
  );
};

export default Profile;
