import { View, Text, SafeAreaView, ActivityIndicator, FlatList } from 'react-native'
import React from 'react'
import { styles } from '../style/styles'
import { ScrollView } from 'react-native-gesture-handler'
import LastItem from '../components/LastItem'
import Header from '../components/Header'
import { useEffect } from 'react'
import { useState } from 'react'
import { hasAuthentication } from '../auth/authentication'
import VerticalEventItem from '../components/VerticalEventItem'
import { useSelector } from 'react-redux'

const maxEventToShow = 5;

const MyEvent = ({ navigation }) => {

  const { organized, participated } = useSelector(state => state.events);
  const [eventOrganized, setEventOrganized] = useState();
  const [eventParticipated, setEventParticipated] = useState();



  useEffect(() => {
    hasAuthentication().then(res => {

      if (!res.isAuthneticated) {
        navigation.replace('Login');
      } else {
        setEventOrganized(organized);
        setEventParticipated(participated);
      }

    }).catch(() => navigation.replace('Login'));

  }, [organized, participated]);

  const emptyListe = () => {
    return (
      <View >
        <Text style={[styles.defaultText, { color: 'black' }]}>Rien à afficher</Text>
      </View>
    )
  }

  const separator = () => {
    return <View style={styles.borderSeparator}></View>;
  }

  return (
    <SafeAreaView style={styles.container}>

      <ScrollView contentContainerStyle={styles.myEvent.container}>
        <Header title={'My Event'} />
        <View style={styles.myEvent.bloc}>
          <Text style={[styles.title, styles.myEvent.title]}>Evènements organisés</Text>
          <View style={styles.myEvent.liste}>
            <FlatList
              style={{ padding: 14, backgroundColor: 'whitesmoke' }}
              ItemSeparatorComponent={separator}
              horizontal={true}
              pagingEnabled={true}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={true}
              data={eventOrganized}
              ListEmptyComponent={() => {
                return eventOrganized ? emptyListe() : <ActivityIndicator color={'orange'} accessibilityLabel='Chargement' size={'large'} />;
              }}
              ListFooterComponent={() => {
                return eventOrganized?.length >= maxEventToShow ? <LastItem action={() => alert("action")} /> : undefined;
              }}
              renderItem={({ item }) => {
                return <VerticalEventItem event={item} />
              }}
            />
          </View>

        </View>

        <View style={styles.myEvent.bloc}>
          <Text style={[styles.title, styles.myEvent.title]}>Evènements Auxquels vous participez</Text>
          <View style={styles.myEvent.liste}>
            <FlatList
              style={{ padding: 14, backgroundColor: 'whitesmoke' }}
              ItemSeparatorComponent={separator}
              horizontal={true}
              pagingEnabled={true}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={true}
              data={eventParticipated}
              ListEmptyComponent={() => {
                return eventParticipated ? emptyListe() : <ActivityIndicator color={'orange'} accessibilityLabel='Chargement' size={'large'} />;
              }}
              ListFooterComponent={() => {
                return eventParticipated?.length >= maxEventToShow ? <LastItem action={() => alert("action")} /> : undefined;
              }}
              renderItem={({ item }) => {
                return <VerticalEventItem event={item} />
              }}

            />
          </View>
        </View>

      </ScrollView>
    </SafeAreaView>
  )
}

export default MyEvent