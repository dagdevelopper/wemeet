import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
} from "react-native";
import React, { useContext, useEffect, useState } from "react";
import Header from "../components/Header";
import {
  blue,
  noirCassé,
  orangeClair,
  primaryColor,
  rougeClair,
  styles,
  vert,
} from "../style/styles";
import {
  DeleteIcon,
  DoneIcon,
  ErrorIcon,
  EventIcon,
  LocationIcon,
  NextIcon,
  PersonIcon,
  PrivateIcon,
  PublicIcon,
  SurveyIcon,
  WarningIcon,
} from "../../assets/icon";
import { useDispatch, useSelector } from "react-redux";
import { hasAuthentication } from "../auth/authentication";
import {
  useEvent,
  useEventRole,
  useInvitation,
  useSurveys,
} from "../ApiCalls/CustomHooks";
import {
  addParticipated,
  deleteEvent,
  removeOrganized,
  removeParticipated,
  subscribe,
  unSubscribe,
} from "../redux_wemeet/Slices/event.slice";
import { setSurvey } from "../redux_wemeet/Slices/survey.slice";
import authProvider from "../auth/authProvider";
import { alreadyParticipate, dateToString } from "../util/utils";
import PopupModal from "../components/PopupModal";
import {
  deleteInvite,
  updateInvite,
} from "../redux_wemeet/Slices/invitation.slice";

const EventDetails = ({ navigation, route }) => {
  const event = route.params.event;
  const dispatch = useDispatch();
  const [userId, setUserId] = useState(undefined);
  const [labelBtn, setLabelBtn] = useState("");
  const [colorTextbtn, setColorTextBtn] = useState("blue");
  const { removeEvent } = useEvent();
  const { subscribeParticipant, unSubscribeParticipant } = useEventRole();
  const [loading, setLoading] = useState(false);
  const { getSurveys } = useSurveys();
  const [DATA, setDATA] = useState();
  const [role, setRole] = useState("");
  const {
    userConnected,
    setIsAuthenticated,
    setOrganized,
    nbEventsOrganized,
    setParticipated,
    nbEventsParticipated,
  } = useContext(authProvider);
  const { surveys } = useSelector((state) => state.surveys);
  const { events } = useSelector((state) => state.events);
  const [participants, setParticipants] = useState([]);
  const [eventAlreadyPassed, setEventPassed] = useState(false);
  const [show, setShow] = useState(false);
  const [showError, setShowError] = useState(false);
  const [msgError, setErrorMsg] = useState("");
  const [msgModal, setMsgModal] = useState("");
  const [msgWarn, setMsgWarn] = useState("");
  const [showWarn, setShowWarn] = useState(false);
  const { updateInvitation, deleteInvitationByEvent } = useInvitation();
  const { invites } = useSelector((state) => state.invitations);
  const getStatus = (id) => {
    const eventFound = events.find((e) => e.id === event.id);
    setParticipants(eventFound.participants);
    const user = eventFound?.participants.find((p) => p.id === id);
    return { status: user?.role };
  };

  useEffect(() => {
    hasAuthentication()
      .then((res) => {
        if (res.isAuthneticated) {
          setUserId(res.userDatas.id);

          const date = new Date(event.date_end);
          const now = new Date(Date.now());
          now.setHours(0, 0, 0, 0);

          setEventPassed(now > date);
          const { status } = getStatus(res.userDatas.id);

          setRole(status);
          if (status === "organizer") {
            setLabelBtn("Supprimer");
          } else if (status === "participant") {
            setLabelBtn("Ne plus participer");
          } else {
            setLabelBtn("Participer");
          }
        } else {
          setIsAuthenticated(false);
          navigation.navigate("Login");
        }
      })
      .catch(() => {
        setIsAuthenticated(false);
        navigation.navigate("Login");
      });

    labelBtn.toLowerCase() === "supprimer" && setColorTextBtn("red");
    labelBtn.toLowerCase() === "ne plus participer" &&
      setColorTextBtn(primaryColor);
    labelBtn.toLowerCase() === "participer" && setColorTextBtn(blue);

    getSurveys(event.id, userConnected.id)
      .then((res) => {
        setDATA(res.data);
        dispatch(setSurvey(res.data));
      })
      .catch((e) => {
        setShowError(!showError);
        setErrorMsg(`Erreur : ${e.message}`);
        console.log("surveys error : ", e);
      });
  }, [labelBtn, event, eventAlreadyPassed, navigation]);

  const handlePressParticipants = () => {
    navigation.navigate("Participants", {
      participants: participants,
      label: event.label,
    });
  };

  const handleInvite = ({ eventId, guest, accepted }) => {
    
    if (alreadyParticipate({ invites, guest, event })) {
      updateInvitation({
        eventId,
        guest,
        accepted,
      })
        .then(() => {
          dispatch(updateInvite({ eventId, accepted }));
        })
        .catch((e) => console.log("error invite : ", e));
    }
  };

  const handleSubscribe = ({ userId, eventId }) => {
    if (!eventAlreadyPassed) {
      setLoading(true);
      handleInvite({ eventId, guest: userConnected.email, accepted: true });
      subscribeParticipant({ userId, eventId })
        .then((res) => {
          setParticipated(nbEventsParticipated + 1);
          dispatch(subscribe(res.data));
          dispatch(addParticipated(event));
          setLoading(false);
          setLabelBtn("Ne plus participer");
        })
        .catch((e) => {
          setShowError(!showError);
          setErrorMsg(`Vous participez déja à ce wemeet !`);
          console.log("error subscribe : ", e);
          setLoading(false);
        });
    } else {
      setMsgWarn("Ce wemeet est déja passé !");
      setShowWarn(!showWarn);
    }
  };

  const handleDeleteEvent = (id) => {
    Alert.alert("Souhaitez-vous supprimer ce Wemeet ?", "", [
      {
        text: "non",
        style: "cancel",
      },
      {
        text: "oui",
        onPress: () => {
          setLoading(true);
          removeEvent({ id })
            .then(() => {
              deleteInvitationByEvent({ eventId: id })
                .then(() => {
                  console.log("invitations deleted");
                })
                .catch((e) =>
                  console.log("error delete all invitations : ", e)
                );

              setShow(!show);
              setMsgModal("wemeet supprimé avec succès ! ");
              dispatch(deleteEvent(event.id));
              dispatch(removeOrganized(event.id));
              setOrganized(nbEventsOrganized > 0 ? nbEventsOrganized - 1 : 0);
              setLoading(false);
            })
            .catch((e) => {
              setShowError(!showError);
              setErrorMsg(`Erreur de suppression : ${e.message}`);
              console.log("remove event error : ", e);
            });
        },
      },
    ]);
  };

  const handleUnsubscribe = ({ userId, eventId }) => {
    setLoading(true);
    handleInvite({ eventId, guest: userConnected.email, accepted: false });
    unSubscribeParticipant({ userId, eventId })
      .then((res) => {
        setParticipated(
          nbEventsParticipated > 0 ? nbEventsParticipated - 1 : 0
        );
        dispatch(unSubscribe(res.data));
        dispatch(removeParticipated(event.id));
        setLoading(false);
        setLabelBtn("Participer");
      })
      .catch((e) => {
        setShowError(!showError);
        setErrorMsg("Vous vous êtes déja désinscrit de ce wemeet !");
        console.log("error subscribe : ", e);
        setLoading(false);
      });
  };

  const handleClickInvite = () => {
    if (eventAlreadyPassed){
      setMsgWarn("Impossible d'inviter à un wemeet déja passé !");
      setShowWarn(!showWarn);
    }else{
      navigation.navigate("Invitation", {participants, event});
    }
  }

  const getTitle = () => {
    return (
      <View style={[{ alignItems: "center", justifyContent: "center" }]}>
        <Text style={[styles.header.title, { alignSelf: "center" }]}>
          {event?.label}
        </Text>
        <View style={styles.field}>
          <Text
            style={[styles.defaultText, { color: "black", marginRight: 2 }]}
          >
            {" "}
            {event?.is_private ? "Privé" : "Public"}{" "}
          </Text>
          <Image source={event?.is_private ? PrivateIcon : PublicIcon} />
        </View>
      </View>
    );
  };
  return (
    <ScrollView>
      <Header title={getTitle()} />
      {eventAlreadyPassed && (
        <View
          style={[
            { flexDirection: "row", justifyContent: "space-around" },
            styles.defaultBorder,
          ]}
        >
          <Text style={[styles.defaultText, { color: noirCassé }]}>
            {" "}
            Ce wemeet a déja eu lieu
          </Text>
          <Image
            style={[styles.icon, { tintColor: "red" }]}
            source={WarningIcon}
          />
        </View>
      )}
      <View>
        <Text style={styles.title}>Description</Text>
        <Text
          style={[
            styles.defaultText,
            { color: "gray" },
            styles.eventDetails.description,
            styles.defaultBorder,
          ]}
        >
          {" "}
          {event?.description}{" "}
        </Text>
        <View style={[styles.defaultBorder, styles.eventDetails.bloc]}>
          <View style={[styles.eventDetails.items]}>
            <Image style={styles.icon} source={LocationIcon} />
            <View style={[styles.button, styles.eventDetails.details.onglets]}>
              <Text style={[styles.defaultText, { color: "black" }]}>
                {event?.city_name}
              </Text>
            </View>
          </View>

          <View style={[styles.eventDetails.items]}>
            <Image style={styles.icon} source={PersonIcon} />
            <TouchableOpacity
              onPressOut={() => handlePressParticipants()}
              style={[styles.button, styles.eventDetails.details.onglets]}
            >
              <View style={styles.eventDetails.details.blocWithLink}>
                <Text style={[styles.defaultText, { color: "black" }]}>
                  {" "}
                  {participants?.length}{" "}
                  {`  Participant${participants?.length > 1 ? "s" : ""}`}
                </Text>
                <Image
                  source={NextIcon}
                  style={[
                    styles.icon,
                    { tintColor: "black", alignSelf: "flex-end" },
                  ]}
                />
              </View>
            </TouchableOpacity>
          </View>

          <View style={[styles.eventDetails.items]}>
            <Image style={styles.icon} source={EventIcon} />
            <TouchableOpacity
              disabled
              style={[styles.button, styles.eventDetails.details.onglets]}
            >
              <View style={styles.eventDetails.details.blocWithLink}>
                <Text style={[styles.defaultText, { color: noirCassé }]}>
                  {" "}
                  {`${dateToString(new Date(event.date_begin))}  ${
                    event.date_begin !== event.date_end
                      ? ` - ${dateToString(new Date(event.date_end))}`
                      : ""
                  }`}{" "}
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={[styles.eventDetails.items]}>
            <Image style={styles.icon} source={SurveyIcon} />
            <TouchableOpacity
              onPressOut={() => {
                navigation.navigate("Sondages", {
                  DATA: DATA,
                  eventId: event.id,
                  role,
                });
              }}
              style={[styles.button, styles.eventDetails.details.onglets]}
            >
              <View style={styles.eventDetails.details.blocWithLink}>
                <Text style={[styles.defaultText, { color: "black" }]}>
                  {surveys?.length}{" "}
                  {`  Sondage${surveys?.length > 1 ? "s" : ""}`}
                </Text>
                <Image
                  source={NextIcon}
                  style={[
                    styles.icon,
                    { tintColor: "black", alignSelf: "flex-end" },
                  ]}
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <PopupModal
          message={msgModal}
          bgColor={orangeClair}
          icon={DoneIcon}
          colorIcon={vert}
          show={show}
          setShow={setShow}
        />
        <PopupModal
          message={msgError}
          bgColor={rougeClair}
          icon={ErrorIcon}
          colorIcon={noirCassé}
          show={showError}
          setShow={setShowError}
        />

        <PopupModal
          message={msgWarn}
          bgColor={"whitesmoke"}
          icon={WarningIcon}
          colorIcon={primaryColor}
          show={showWarn}
          setShow={setShowWarn}
        />

        <View
          style={{
            padding: 5,
            justifyContent: "space-around",
            flexDirection: "row",
            marginTop: 2,
          }}
        >
          {
            role === 'organizer' && <TouchableOpacity onPressOut={() => handleClickInvite()}>
              <Text style={[styles.subTitle, { color: blue }]}>Inviter</Text>
            </TouchableOpacity>
          }

          <TouchableOpacity
            onPressOut={() => {
              if (labelBtn.toLowerCase() === "supprimer") {
                handleDeleteEvent(event.id);
              } else if (labelBtn.toLowerCase() === "participer") {
                handleSubscribe({ userId, eventId: event.id });
              } else {
                handleUnsubscribe({ userId, eventId: event.id });
              }
            }}
            style={[
              // styles.field,
              {
                marginBottom: 93,
                // width: "30%",
                flexDirection: "row",
                alignSelf: "center",
                // height: "8%",
                alignItems: "center",
              },
            ]}
            disabled={loading}
          >
            {loading && (
              <ActivityIndicator
                size={"large"}
                color={"black"}
                style={{ position: "absolute" }}
              />
            )}
            {labelBtn.toLowerCase() === "supprimer" && (
              <Image source={DeleteIcon} style={[{ tintColor: "red" }]} />
            )}
            <Text style={[styles.subTitle, { color: colorTextbtn }]}>
              {" "}
              {labelBtn}{" "}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default EventDetails;
