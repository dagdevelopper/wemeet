import {  Text, TouchableOpacity, KeyboardAvoidingView, TextInput, Button, Alert } from 'react-native'
import React, { useState, useEffect } from 'react'
import { styles, noirCassé, rougeClair } from '../style/styles'
import { useSurveys } from '../ApiCalls/CustomHooks';
import { useDispatch } from 'react-redux';
import { addSurveyState, deleteSurveyState, updateSurveyState } from '../redux_wemeet/Slices/survey.slice';
import PopupModal from '../components/PopupModal';
import { ErrorIcon } from '../../assets/icon';

const NewSurvey = ({ route, navigation }) => {
  const [eventId, setEventId] = useState();
  const [id, setId] = useState();
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [errorMsg, setErrorMsg] = useState('');
  const { createSurvey, deleteSurvey, updateSurvey } = useSurveys();
  const dispatch = useDispatch();
  const [showError, setShowError] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    setEventId(route.params.eventId);
    setTitle(route.params.title);
    setDescription(route.params.description);
    setId(route.params.id);

  }, [route]);

  const validForm = () => {
    let valid = true;

    if (title === '') {
      valid = false;
      setErrorMsg('Le titre est obligatoire !');
    }
    return valid;
  }

  const handleDelete = () => {
    Alert.alert(
      `${title}`,
      'Supprimer le sondage ?',
      [
        {
          text: 'annuler',
          style: 'cancel'
        },
        {
          text: 'supprimer',
          onPress: () => {
            deleteSurvey({ id }).then(() => {
              dispatch(deleteSurveyState(id));
            }).catch(e => {
              setShowError(!showError);
              setError(`Erreur de suppression : ${e.message}`);
              console.error("error delete survey : ", e)
            });
            navigation.goBack();
          }
        }
      ]
    );
  }

  const addSurvey = () => {
    if (validForm()) {
      createSurvey({ event: eventId, title, description }).then(res => {

        dispatch(addSurveyState(res.data));
        setTitle('');
        setDescription('');
        navigation.goBack();

      }).catch(e => {
        setShowError(!showError);
        setError(`Erreur de création du sondage : ${e.message}`);
        console.error("error add survey : ", e)
      });

    }
  }

  const handleUpdate = () => {
    if (validForm()) {
      updateSurvey({ id, title, description }).then(() => {

        dispatch(updateSurveyState({ title, description, id }));
        navigation.goBack();

      }).catch(e => {
        setShowError(!showError);
        setError(`Erreur de la mise à jour du sondage : ${e.message}`);
        console.log("error update survey : ",e);
      });

    }

  }


  return (
    <KeyboardAvoidingView behavior='padding' style={{ alignItems: 'center', backgroundColor: 'white', height: '100%' }}>
      <TextInput
        value={title}
        onChangeText={t => setTitle(t)}
        style={[styles.input, styles.defaultText, { marginBottom: 12, color: 'black' }]}
        placeholder={"Titre*"}
        placeholderTextColor={noirCassé}
        onFocus={() => setErrorMsg('')}
      />

      {errorMsg !== '' && <Text style={[styles.error, { marginBottom: 7 }]}> {errorMsg} </Text>}

      <TextInput style={[styles.input, styles.defaultText, { minHeight: '10%', color: 'black' }]}
        value={description}
        onChangeText={d => setDescription(d)}
        placeholder={"Description (Facultative)"}
        placeholderTextColor={noirCassé} multiline
        onFocus={() => setErrorMsg('')}
      />

      <PopupModal
        message={error}
        bgColor={rougeClair}
        icon={ErrorIcon}
        colorIcon={noirCassé}
        show={showError}
        setShow={setShowError}
      />

      <TouchableOpacity onPressOut={route.params.toUpdate ? handleUpdate : addSurvey} style={[styles.button, { width: '85%' }]}>
        <Text style={[styles.subTitle, { color: noirCassé }]}> {route.params.toUpdate ? 'Modifier' : 'Ajouter'} </Text>
      </TouchableOpacity>
      {route.params.toUpdate && <TouchableOpacity onPressOut={handleDelete}>
        <Text style={[styles.subTitle, { color: 'red', fontWeight: 'normal' }]}> Supprimer </Text>
      </TouchableOpacity>}

    </KeyboardAvoidingView>
  )
}

export default NewSurvey