import React, { useEffect, useState } from "react";
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  Pressable,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { noirCassé, rougeClair, styles } from "../style/styles";
import Header from "../components/Header";
import FadeInView from "../components/FadeInView";
import {
  CloseIcon,
  ErrorIcon,
  LogoIcon,
  MailIcon,
  PasswordIcon,
} from "../../assets/icon";
import { login } from "../auth/authentication";
import HidePassword from "../components/HidePassword";
import { useContext } from "react";
import Auth from "../auth/authProvider";
import { useEventRole, useInvitation } from "../ApiCalls/CustomHooks";
import PopupModal from "../components/PopupModal";
import { ActivityIndicator } from "react-native";
import { useDispatch } from "react-redux";
import { setinvitations } from "../redux_wemeet/Slices/invitation.slice";

const Login = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [msgError, setMsgError] = useState(
    "E-mail ou mot de passe incorrect !"
  );
  const [errorConnection, setErrorConnection] = useState(false);
  const [secureText, setSecureText] = useState(true);
  const {
    setIsAuthenticated,
    setUserConnected,
    isAuthenticated,
    setOrganized,
    setParticipated,
  } = useContext(Auth);
  const [error, setError] = useState("");
  const [showError, setShowError] = useState(false);
  const { getMyEvents } = useEventRole();
  const [loading, setLoading] = useState(false);
  const { getInvitations } = useInvitation();
  const dispatch = useDispatch();

  useEffect(() => {
    if (isAuthenticated) {
      navigation.navigate("Home");
    }
  }, [isAuthenticated]);

  const handleConnect = () => {
    setLoading(true);
    login({ email, password })
      .then((data) => {
        setUserConnected(data);
        setIsAuthenticated(true);
        setLoading(false);

        getMyEvents(data.id)
          .then((data) => {
            const { participated, organized } = data.data;
            setOrganized(organized.length);
            setParticipated(participated.length);
          })
          .catch((e) => console.log("erreur get MyEvent : ", e));

        getInvitations({ email: data.email })
          .then((res) => {
            dispatch(setinvitations(res.data));
          })
          .catch((e) => {
            console.log("invitation loading error : ", e);
            setIsAuthenticated(false);
            navigation.navigate("Login");
          });

        setPassword("");
        setEmail("");
        setErrorConnection(false);
        navigation.navigate("Home");
      })
      .catch((e) => {
        console.log("error login : ", e);
        setLoading(false);
        if (e.code === "ERR_BAD_REQUEST") setErrorConnection(true);
        else {
          setShowError(true);
          setError("Erreur connexion au serveur !");
        }
      });
  };

  const handleClick = () => {
    navigation.navigate("Registration");
  };

  return (
    <FadeInView duration={1000}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{ backgroundColor: "white" }}
      >
        <ScrollView nestedScrollEnabled>
          <View style={[styles.container]}>
            <Header title={"Wemeet"} />
            <View style={styles.logo.view}>
              <Image style={styles.logo.image} source={LogoIcon} />
            </View>
            <PopupModal
              message={error}
              icon={ErrorIcon}
              bgColor={rougeClair}
              colorIcon={"black"}
              colorMsg={noirCassé}
              show={showError}
              setShow={setShowError}
            />
            <View>
              <FadeInView duration={1000} style={styles.field}>
                {errorConnection && (
                  <Text style={[styles.text, styles.error]}> {msgError} </Text>
                )}
              </FadeInView>
              <View style={styles.field}>
                <Image style={styles.icon} source={MailIcon} />
                <TextInput
                  style={styles.input}
                  textContentType="emailAddress"
                  value={email}
                  onChangeText={(e) => setEmail(e)}
                  placeholder={"Adresse mail"}
                />
              </View>
              <View style={styles.field}>
                <Image style={styles.icon} source={PasswordIcon} />
                <TextInput
                  secureTextEntry={secureText}
                  textContentType="password"
                  style={styles.input}
                  value={password}
                  onChangeText={(p) => setPassword(p)}
                  placeholder={"Mot de passe"}
                />
                <HidePassword
                  secureText={secureText}
                  setSecureText={setSecureText}
                />
              </View>
            </View>
            <TouchableOpacity onPress={() => handleConnect()}>
              <View style={[styles.button]}>
                {!loading ? (
                  <Text style={styles.text}> Se connecter </Text>
                ) : (
                  <ActivityIndicator />
                )}
              </View>
            </TouchableOpacity>

            <Pressable onPress={() => handleClick()}>
              <Text style={[styles.link]}>
                Besoin d'un compte ? S'inscrire !
              </Text>
            </Pressable>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </FadeInView>
  );
};

export default Login;
