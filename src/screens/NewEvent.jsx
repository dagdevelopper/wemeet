import { View, Text, Image, ScrollView, KeyboardAvoidingView, TextInput, TouchableOpacity, ActivityIndicator, Pressable } from 'react-native';
import React, { useContext, useState, useEffect } from 'react';
import { primaryColor, styles, orangeClair, noirCassé, vert, rougeClair } from '../style/styles';
import FadeInView from '../components/FadeInView';
import Header from '../components/Header';
import { DoneIcon, ErrorIcon, EventIcon, EventUnknown, NextIcon } from '../../assets/icon';
import CheckBox from 'react-native-check-box';
import { hasAuthentication } from '../auth/authentication';
import { useDispatch } from 'react-redux';
import { addOrganized, createEvent } from '../redux_wemeet/Slices/event.slice';
import { useEvent } from '../ApiCalls/CustomHooks';
import DatePicker from 'react-native-neat-date-picker';
import authProvider from '../auth/authProvider';
import PopupModal from '../components/PopupModal';

const defaultPadding = 11;
let today = new Date(Date.now());
today.setHours(0, 0, 0, 0);

const NewEvent = ({ navigation }) => {
  const [error, setError] = useState('');
  const [show, setShow] = useState(false);
  const [msgModal, setMsgModal] = useState('');
  const [showError, setShowError] = useState(false);
  const [label, setLabel] = useState('');
  const [description, setDescription] = useState('');
  const [location, setLocation] = useState('');
  const [isPrivate, setIsPrivate] = useState(false);
  const [labelError, setLabelErr] = useState(null);
  const [descriptionError, setDescriptionError] = useState(null);
  const [dateError, setDateError] = useState(null);
  const [locationError, setLocationError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [dateBegin, setDateBegin] = useState('');
  const [dateEnd, setDateEnd] = useState();
  const [openDatePicker, setOpenDatePicker] = useState(false);
  const { userConnected, setOrganized, nbEventsOrganized } = useContext(authProvider);
  const dispatch = useDispatch();
  const { newEvent } = useEvent();

  

  useEffect(() => {
    hasAuthentication().then(res => {
      if (!res.isAuthneticated) {
        navigation.replace('Login');
      }
    }).catch(() => navigation.replace('Login'));
  }, []);

  const containsError = () => {

    let hasError = false;
    if (label.trim() === '') {
      setLabelErr('Ce champ est obligatoire !');
      hasError = true;
    } else setLabelErr(null)

    if (description.trim() === '') {
      setDescriptionError('Ce champ est obligatoire !');
      hasError = true;
    } else setDescriptionError(null)

    if (location.trim() === '') {
      setLocationError('Ce champ est obligatoire !');
      hasError = true;
    } else setLocationError(null)

    if (dateBegin.trim() === '') {
      setDateError('Ce champ est obligatoire !');
      hasError = true;
    } else setDateError(null)

    return hasError;
  }

  const initErrorMsg = () => {

    setDescriptionError(null);
    setLocationError(null);
    setLabelErr(null);
    setLoading(false);
    setDateError(null);
  }

  const handleSaveEvent = () => {
    if (!containsError()) {
      setLoading(true);

      newEvent({ email: userConnected.email, label, description, isPrivate, location, dateBegin, dateEnd }).then(res => {
       
        dispatch(createEvent({ event: res.data.createdEvent }));
        dispatch(addOrganized(res.data.createdEvent));

        setLabel('');
        setDescription('');
        setLocation('');
        initErrorMsg();
        setDateBegin('');
        setDateEnd('');
        setOrganized(nbEventsOrganized + 1);

        setShow(!show);
        setMsgModal('Wemeet créé avec succès ! ');

      }).catch(e => {
        setLoading(false);
        setShowError(!showError);
        setError(`Erreur de création d'un nouveau wemeet : ${e.message}`);
        console.log("error add event : ", e)
      })
    }
  }

  return (

    <FadeInView duration={600} >
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} >
        <ScrollView nestedScrollEnabled={true} >
          <View style={{ marginBottom: 120 }}>
            <Header title={`Créer un Wemeet`} />
            <View style={{ padding: defaultPadding }}>
              <View style={styles.newEvent.description}>
                <Image source={EventUnknown} style={styles.icon} />
                <Text style={[styles.defaultText, { color: 'black', paddingLeft: 3, paddingRight: 3 }]}>
                  Créez votre premier Wemeet afin de le partager entre amis, ou personnes avec les mêmes délires que vous !
                </Text>
              </View>

              <View style={{ backgroundColor: 'whitesmoke', paddingLeft: defaultPadding }}>
                <Text style={[styles.defaultText, { padding: defaultPadding, color: 'black', fontWeight: '300' }]}>Titre de l'évènement</Text>
                <TextInput
                  onFocus={initErrorMsg}
                  style={styles.input}
                  value={label}
                  onChangeText={(f) => setLabel(f)}
                  placeholder={'Titre*'}
                  textContentType='name'
                />
                {labelError && <Text style={[styles.error, { padding: 2 }]}> {labelError} </Text>}

                <Text style={[styles.defaultText, { padding: defaultPadding, color: 'black', fontWeight: '300' }]}>Description</Text>
                <TextInput
                  onFocus={initErrorMsg}
                  style={[styles.input, { height: '20%', maxHeight: '30%' }]}
                  value={description}
                  multiline

                  onChangeText={(f) => setDescription(f)}
                  placeholder={'Description*'}
                  textContentType='name'
                />
                {descriptionError && <Text style={[styles.error, { padding: 2 }]}> {descriptionError} </Text>}


                <Text style={[styles.defaultText, { padding: defaultPadding, color: 'black', fontWeight: '300' }]}>Date</Text>
                <View style={[{ flexDirection: 'row', alignItems: 'center' }]}>
                  <TextInput
                    onFocus={initErrorMsg}
                    style={[styles.input, { fontSize: 15 }]}
                    value={dateBegin ? `${dateEnd !== dateBegin ? ` ${dateBegin} -> ${dateEnd}` : `${dateBegin}`} ` : ''}
                    onChangeText={(f) => setDescription(f)}
                    placeholder={'Choisir une date*'}
                    editable={false}
                  />
                  <Pressable onPress={() => setOpenDatePicker(!openDatePicker)}><Image source={EventIcon} style={[styles.icon, { position: 'absolute', tintColor: 'black', right: 6, top: -12 }]} /></Pressable>
                </View>
                {dateError && <Text style={[styles.error, { padding: 2 }]}> {dateError} </Text>}

                <DatePicker
                  isVisible={openDatePicker}
                  mode={'range'}
                  initialDate={new Date(Date.now())}
                  minDate={today}
                  onCancel={() => setOpenDatePicker(!openDatePicker)}
                  onConfirm={(date) => {
                    setDateBegin(date.startDateString)
                    setDateEnd(date.endDateString)
                    setOpenDatePicker(!openDatePicker)
                  }}

                  dateStringFormat={'yyyy-mm-dd'}
                  modalStyles={styles.datePicker}
                  colorOptions={{
                    weekDaysColor: primaryColor,
                    headerColor: orangeClair,
                    selectedDateBackgroundColor: primaryColor,
                    confirmButtonColor: primaryColor,
                    headerTextColor: noirCassé,
                    changeYearModalColor: primaryColor,
                  }}
                />

                <Text style={[styles.defaultText, { padding: defaultPadding, color: 'black', fontWeight: '300' }]}>Location</Text>
                <TextInput
                  onFocus={initErrorMsg}
                  style={[styles.input]}
                  value={location}
                  onChangeText={(f) => setLocation(f)}
                  placeholder={'ville ou adresse*'}
                  textContentType='fullStreetAddress'
                />
                {locationError && <Text style={[styles.error, { padding: 2 }]}> {locationError} </Text>}


                <View>
                  <CheckBox
                    isChecked={!isPrivate}
                    onClick={() => setIsPrivate(!isPrivate)}
                    checkBoxColor={primaryColor}
                    style={{ padding: defaultPadding }}
                    rightTextView={<Text style={[styles.defaultText, { padding: defaultPadding, color: 'black', fontWeight: '300' }]}>Rendre public</Text>}
                  />

                </View>
                <Text style={[styles.text, { padding: defaultPadding, color: 'black', fontWeight: '200', fontStyle: 'italic' }]}>* champs obligatoires</Text>
                <TouchableOpacity disabled={loading} onPress={handleSaveEvent}>
                  <View style={[styles.field, styles.button, { backgroundColor: loading ? orangeClair : primaryColor }]}>
                    {loading && <ActivityIndicator color={'black'} size={'small'} style={{ marginRight: 7 }} />}
                    <Text style={styles.text}>Créer </Text>
                    <Image source={NextIcon} style={[styles.icon, { tintColor: 'black' }]} />
                  </View>
                </TouchableOpacity>

                <PopupModal
                  message={msgModal}
                  bgColor={orangeClair}
                  icon={DoneIcon}
                  colorIcon={vert}
                  show={show}
                  setShow={setShow}
                />
                <PopupModal
                  message={error}
                  bgColor={rougeClair}
                  icon={ErrorIcon}
                  colorIcon={noirCassé}
                  show={error}
                  setShow={setShowError}
                />
              </View>
            </View>
          </View>
        </ScrollView>

      </KeyboardAvoidingView>
    </FadeInView>

  )
}

export default NewEvent;