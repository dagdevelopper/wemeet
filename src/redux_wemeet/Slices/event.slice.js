import { createSlice } from '@reduxjs/toolkit';

export const eventSlice = createSlice({
    name: 'eventSlice',
    initialState: {
        events: null,
        eventsToShow: null,
        loading: false,
        participated: null,
        organized: null
    },

    reducers: {
        updateListEvents: (state, action) => {
            let eventsToShow = [];
            if (action.payload !== "") {
                
                eventsToShow = state.events?.filter(
                     e => e.label.toLowerCase().includes(action.payload.toLowerCase()) ||
                        e.city_name.toLowerCase().includes(action.payload.toLowerCase()) ||
                        e.description.toLowerCase().includes(action.payload.toLowerCase())
                );
                state.eventsToShow = eventsToShow;
            }

        },

        setEvents: (state, action) => {
            state.events = action.payload;
            state.eventsToShow = state.events;
        },
        createEvent: (state, action) => {
            let { event } = action.payload;
            state.events?.unshift(event);
        },
        deleteEvent: (state, action) => {
            state.events = state.events.filter(e => e.id !== action.payload);
        },
        subscribe: (state, action) => {
            const { user, eventId } = action.payload;
        
            for (let e of state.events) {
                if (e.id === eventId) {
                    user.role = 'participant';
                    e.participants.push(user);
                    break;
                }
            }
        },
        unSubscribe: (state, action) => {

            const { userId, eventId } = action.payload;
            for (let e of state.events) {

                if (e.id === eventId) {
                    e.participants = e.participants.filter(p => p.id !== userId)
                    break;
                }
            }
        },
        setMyEvents: (state, action) => {
            state.organized = action.payload.organized.reverse();
            state.participated = action.payload.participated.reverse();
        },
        addOrganized: (state, action) => {
            state.organized.unshift(action.payload);
        },
        removeOrganized: (state, action) => {
            state.organized = state.organized.filter(e => e.id !== action.payload).reverse();

        },
        addParticipated: (state, action) => {
            state.participated.unshift(action.payload);
        },
        removeParticipated: (state, action) => {
            state.participated = state.participated.filter(e => e.id !== action.payload).reverse();
        }, 
        resetStateEvent : (state) => {
            state.events = null;
            state.eventsToShow = null;
            state.organized = null;
            state.participated = null;
        }

    },

})

export const { updateListEvents, setEvents,
    createEvent, deleteEvent, subscribe, unSubscribe, setMyEvents, addOrganized, addParticipated, removeOrganized, removeParticipated, resetStateEvent
} = eventSlice.actions;