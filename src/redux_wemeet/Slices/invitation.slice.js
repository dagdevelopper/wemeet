import { createSlice } from '@reduxjs/toolkit';

export const InvitationSlice = createSlice({
    name : 'invitationSlice',
    initialState : {
        invites : null,
        nbUnread : 0
    },
    reducers : {
        setinvitations : (state, action) => {
            const {invites, nbUnread} = action.payload;
            state.invites = invites;
            state.nbUnread = nbUnread;
        },
       
        updateInvite : (state, action) => {
            const {eventId, accepted} = action.payload;

            state.nbUnread += accepted ? -1 : 1; 

            state.invites.forEach(item => {
                item.invitations.forEach(i => {
                    if (i.event.id === eventId)
                        i.accepted = accepted;
                });
            });

        },
        deleteInvite : (state, action) => {
            const {eventId} = action.payload;

            state.invites.forEach(item => {
                item.invitations = item.invitations.filter(i => i.event.id !== eventId);
            });
            state.nbUnread--;
        },
        resetStateInvite : (state) => {
            state.invites = null;
            state.nbUnread = 0;
        }
    }
});

export const {deleteInvite, updateInvite, setinvitations, resetStateInvite} = InvitationSlice.actions;