import { createSlice } from "@reduxjs/toolkit";


export const surveySlice = createSlice({
    name: 'surveySlice',
    initialState: {
        surveys: []
    },
    reducers: {
        setSurvey: (state, action) => {
            state.surveys = action.payload;
        },
        addSurveyState : (state, action) => {
            const {createdSurvey} = action.payload;
            state.surveys.push(createdSurvey);
        },
        deleteSurveyState : (state, action) => {
            state.surveys = state.surveys.filter(s => s.id !== action.payload);
        },
        updateSurveyState : (state, action) => {
            const {id, title, description} = action.payload;
            
            let iSurvey = 0;
            while (iSurvey < state.surveys.length && state.surveys[iSurvey].id !== id) {
                iSurvey++;
            }
            
            state.surveys[iSurvey].title = title;
            state.surveys[iSurvey].description = description;
        },
        updateSurveyLineState: (state, action) => {
            let iSurvey = 0;
            const updatedSurveyLine = action.payload.updatedSurveyLine;

            while (iSurvey < state.surveys.length && state.surveys[iSurvey].id !== updatedSurveyLine.survey) {
                iSurvey++;
            }

            if (iSurvey < state.surveys.length) {
               
                let sl = 0;
                while (sl < state.surveys[iSurvey].surveyLines.length && state.surveys[iSurvey].surveyLines[sl].line_number !== updatedSurveyLine.line_number) {
                    sl++;
                }
                sl < state.surveys[iSurvey].surveyLines.length && (state.surveys[iSurvey].surveyLines[sl] = updatedSurveyLine);
            } else
                console.error("id survey non trouvé dans le state");
        },
        addSurveyLineState : (state, action) => {
            const {createdSurveyLine} = action.payload;
           
            let iSurvey = 0;
            while (iSurvey < state.surveys.length && state.surveys[iSurvey].id !== createdSurveyLine.survey) {
                iSurvey++;
            }
            state.surveys[iSurvey].surveyLines.push(createdSurveyLine);
        },
        deleteSurveyLineState : (state, action) => {
            let iSurvey = 0;
            const {survey, line_number} = action.payload;

            while (iSurvey < state.surveys.length && state.surveys[iSurvey].id !== survey) {
                iSurvey++;
            }
            state.surveys[iSurvey].surveyLines = state.surveys[iSurvey].surveyLines.filter(sl => sl.line_number !== line_number);
        },
        resetStateSurvey : (state) => {
            state.surveys = [];
        }
    }
});

export const { setSurvey, updateSurveyLineState, addSurveyLineState, addSurveyState, updateSurveyState, deleteSurveyState, deleteSurveyLineState, resetStateSurvey } = surveySlice.actions;