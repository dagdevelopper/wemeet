import { configureStore } from "@reduxjs/toolkit";
import { eventSlice } from "./Slices/event.slice";
import { surveySlice } from "./Slices/survey.slice";
import { InvitationSlice } from "./Slices/invitation.slice";


export const store = configureStore({
    reducer : {
        events : eventSlice.reducer,
        surveys : surveySlice.reducer,
        invitations : InvitationSlice.reducer,
    }  
});

