import { Dimensions, Platform, StatusBar, StyleSheet } from "react-native";

export const primaryColor = "orange";
export const orangeClair = "rgb(235, 204, 163)";
export const rougeClair = "rgb(242, 100, 107)";
export const blue = "rgb(43, 149, 255)";
export const noirCassé = "rgb(74, 74, 74)";
export const vert = "rgb(0, 159, 0)";

const iconDimensions = 24;
const defaultFontSize = 16;
const borderRadiusLengthProfile = 90;
const borderRadiusLengthMyEvent = 54;
const defaultRadius = 19;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    height: Dimensions.get("window").height,
    //width : "60%"
   
  },
  invitation: {
    container: {
      backgroundColor: "#fff",
      alignItems: "flex-start",
      height: Dimensions.get("window").height,
    },
    bloc: {
      margin: 9,
    },
    blockWithImage: {
      flexDirection: "row",
      marginLeft: 6,
    },
  },
  participants: {
    item: {
      flexDirection: "row",
      justifyContent: "flex-start",
      alignItems: "center",
    },
  },
  platformSafeView: {
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  error: {
    fontWeight: "400",
    fontStyle: "italic",
    color: "red",
  },
  logoPage: {
    container: {
      backgroundColor: "transparent",
      flexDirection: "row",
      alignItem: "center",
      justifyContent: "center",
    },
    text: {
      fontSize: 45,
      alignSelf: "center",
      paddingLeft: 5,
    },
    logo: {
      borderTopLeftRadius: borderRadiusLengthProfile,
      borderBottomRightRadius: borderRadiusLengthProfile,
      borderTopLeftRadius: borderRadiusLengthProfile,
      width: 125,
      height: 125,
    },
  },
  login: {
    container: {},
  },
  defaultText: {
    fontSize: 19,
    fontWeight: "200",
    color: "whitesmoke",
  },
  timeIndicator: {
    container: {
      textAlign: "flex-end",
      justifyContent: "center",
    },
  },
  shadow: {
    shadowOpacity: 0.8,
  },
  home: {
    container: {
      flex: 1,
      justifyContent: "center",
    },
  },
  myEvent: {
    container: {},
    bloc: {
      marginTop: 12,
    },
    title: {
      fontSize: 27,
      paddingLeft: 20,
      paddingRight: 28,
      marginTop: 12,
    },
    liste: {
      paddingTop: 24,
      paddingBottom: 34,
      width: "100%",
    },
    seeMore: {
      backgroundColor: orangeClair,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 30,
      marginBottom: 20,
      width: "100%",
      height: 100,
      padding: 20,
      borderBottomEndRadius: borderRadiusLengthMyEvent,
      borderBottomStartRadius: borderRadiusLengthMyEvent,
      borderTopLeftRadius: borderRadiusLengthMyEvent,
      borderTopRightRadius: borderRadiusLengthMyEvent,
    },
  },
  newEvent: {
    description: {
      padding: 11,
      flexDirection: "row",
      borderWidth: 0.2,
      marginBottom: 11,
      borderBottomEndRadius: defaultRadius,
      borderBottomStartRadius: defaultRadius,
      borderTopLeftRadius: borderRadiusLengthMyEvent,
      borderTopRightRadius: borderRadiusLengthMyEvent,
      borderColor: primaryColor,
    },
  },
  subTitle: {
    fontSize: 19,
    fontWeight: "500",
  },
  title: {
    fontSize: 24,
    fontWeight: "600",
    margin: 12,
  },
  eventDetails: {
    description: {
      width: "95%",
      alignSelf: "center",
      padding: 8,
      margin: 19,
    },
    items: {
      flexDirection: "row",
      alignItems: "center",
    },
    details: {
      blocWithLink: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
      },
      onglets: {
        width: "90%",
        marginLeft: 4,
        alignItems: "flex-start",
        paddingLeft: 7,
      },
    },
  },
  defaultBorder: {
    padding: 12,
    margin: 8,
    borderWidth: 0.4,
    borderColor: primaryColor,
    borderBottomEndRadius: defaultRadius,
    borderBottomStartRadius: defaultRadius,
    borderTopLeftRadius: defaultRadius,
    borderTopRightRadius: defaultRadius,
  },
  profile: {
    container: {},
    header: {
      container: {
        width: "100%",
        height: 400,
        alignItems: "center",
        backgroundColor: orangeClair,
        paddingTop: 83,
      },

      image: {
        width: 80,
        height: 80,
        borderBottomLeftRadius: borderRadiusLengthProfile,
        borderBottomRightRadius: borderRadiusLengthProfile,
        borderTopRightRadius: borderRadiusLengthProfile,
        borderTopLeftRadius: borderRadiusLengthProfile,
        margin: 13,
      },

      name: {
        fontSize: 27,
        fontWeight: "600",
        color: "black",
        backgroundColor: "transparent",
        shadowOpacity: 0.5,
        shadowColor: orangeClair,
      },
    },
    detail: {
      container: {
        padding: 12,
        backgroundColor: "whitesmoke",
      },
      item: {
        borderBottomWidth: 0.2,
        paddingBottom: 8,
        marginBottom: 19,
        paddingTop: 5,
      },
    },

    recap: {
      container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: "100%",
        padding: 30,
      },

      item: {
        alignItems: "center",
        number: {
          fontWeight: "700",
          color: "white",
          fontSize: 25,
        },
      },
    },
    body: {
      container: {
        width: "100%",
      },
      onglet: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        margin: 12,
        borderBottomWidth: 0.2,
        paddingBottom: 16,
        borderColor: primaryColor,
      },
      deconnexion: {
        backgroundColor: orangeClair,
        width: "90%",
        alignSelf: "center",
        alignItems: "center",
        flexDirection: "row",
      },
    },
  },
  defaultRoundedBorder: {
    borderBottomLeftRadius: borderRadiusLengthProfile,
    borderBottomRightRadius: borderRadiusLengthProfile,
    borderTopRightRadius: borderRadiusLengthProfile,
    borderTopLeftRadius: borderRadiusLengthProfile,
  },
  header: {
    container: {
      backgroundColor: primaryColor,
      width: Dimensions.get("window").width,
      flex: 0,
      height: 210,
      marginTop: StatusBar.currentHeight,
      marginBottom: 30,
      borderBottomStartRadius: 19,
      borderBottomEndRadius: 19,
      alignItems: "center",
      justifyContent: "center",
    },
    title: {
      fontSize: 36,
      fontWeight: "bold",
      color: orangeClair,

      alignItems: "center",
      justifyContent: "center",
    },
  },
  event_component: {
    container: {
      //alignItems: "flex-start",
      justifyContent: "center",
      marginTop: 20,
      paddingLeft: 13,
      paddingRight: 13,
      backgroundColor: "transparent",
      // shadowColor: "#000",
      // shadowOffset: {
      //     width: 5,
      //     height: 5,
      // },
      // shadowOpacity: 3,
      // shadowRadius: 34,
      // elevation: 4,
    },
    label: {
      fontSize: 22,
      fontWeight: "400",
      color: "black",
    },

    description: {
      color: "darkgray",
      padding: 9,
      width: "99%",
      alignItems: "flex-start",
      justifyContent: "center",
    },
    city: {
      fontWeight: "thin",
      color: primaryColor,
      fontSize: 20,
    },
    dateCreation: {
      fontSize: 12,
      color: noirCassé,
      alignSelf: "center",
    },
    date: {},
  },
  eventListe: {
    container: {
      height: "40%",
      flex : 1
    },
  },
  badge: {
    backgroundColor: primaryColor,
    marginLeft: 12,
    borderBottomLeftRadius: defaultRadius,
    borderBottomRightRadius: defaultRadius,
    borderTopRightRadius: defaultRadius,
    borderTopLeftRadius: defaultRadius,
    justifyContent: "center",
  },
  icon: {
    width: iconDimensions,
    height: iconDimensions,
    tintColor: orangeClair,
  },
  iconSelected: {
    width: iconDimensions + 5,
    height: iconDimensions + 5,
    tintColor: "black",
  },
  logo: {
    image: {
      width: 190,
      height: 95,
    },
    view: {
      width: Dimensions.get("window").width,
      alignItems: "center",
      height: 150,
      marginTop: 30,
    },
  },
  horizontalLine: {
    borderWidth: 0.17,
    borderColor: primaryColor,
    borderTopLeftRadius: 83,
    borderBottomLeftRadius: 83,
    width: "80%",
    alignSelf: "center",
    shadowOpacity: 0.3,
    marginTop: 4,
    height: 1,
    overflow: "hidden",
  },
  borderSeparator: {
    borderLeftWidth: 1,
    borderLeftColor: primaryColor,
    borderTopLeftRadius: 83,
    borderBottomLeftRadius: 83,
    height: "70%",
    alignSelf: "center",
    shadowOpacity: 0.1,
    marginRight: 4,
  },
  button: {
    backgroundColor: primaryColor,
    color: "black",
    width: Dimensions.get("window").width - 200,
    marginTop: 30,
    height: 40,
    justifyContent: "center",
    borderRadius: 19,
    marginBottom: 20,
    alignItems: "center",
  },
  datePicker: {
    flex: 1,
  },
  input: {
    backgroundColor: "lightgray",
    justifyContent: "center",
    marginLeft: 6,
    width: "85%",
    height: 40,
    letterSpacing: 2,
    padding: 9,
    borderRadius: 13,
    fontSize: defaultFontSize,
  },

  field: {
    flexDirection: "row",
    justifyContent: "center",
    padding: 5,
    marginTop: 4,
    width: "100%",
  },
  link: {
    textDecorationLine: "underline",
    color: primaryColor,
    marginTop: 22,
  },
  text: {
    fontSize: defaultFontSize,
  },
  eyes: {
    tintColor: "black",
    position: "absolute",
    right: 42,
    top: 12,
  },
  menuBar: {
    container: {
      justifyContent: "space-between",
      alignItems: "center",
      flexDirection: "row",
      backgroundColor: primaryColor,
      height: "8%",
      paddingLeft: 4,
      paddingRight: 4,
    },
    elements: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
    },
  },
  headerNav: {
    container: {
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      width: "100%",
      padding: 12,
      position: "absolute",
      top: 45,
      borderBottomWidth: 0.3,
      backgroundColor: "transparent",
      borderBottomStartRadius: 23,
      borderBottomEndRadius: 23,
    },
    element: {
      color: primaryColor,
      fontSize: 18,
    },
    rightElement: {
      fontSize: 18,
    },
    title: {
      fontSize: 23,
      fontWeight: "semiBold",
      alignItem: "center",
    },
  },
  searchBar: {
    container: {
      margin: 15,
      justifyContent: "flex-start",
      alignItems: "center",
      flexDirection: "row",
      width: "90%",
    },
    input: {
      fontSize: 18,
      marginLeft: 10,
      width: "90%",
      color: noirCassé,
    },
    unclicked: {
      padding: 5,
      flexDirection: "row",
      width: "80%",
      backgroundColor: "whitesmoke",
      borderRadius: 19,
      alignItems: "center",
    },
    clicked: {
      padding: 2,
      flexDirection: "row",
      width: "50%",
      backgroundColor: "#d9dbda",
      borderRadius: 19,
      alignItems: "center",
      justifyContent: "space-evently",
    },
  },
  password: {
    good: {
      backgroundColor: "lightgreen",
    },
    bad: {
      backgroundColor: rougeClair,
    },
  },
  surveyItem: {
    container: {
      justifyContent: "flex-end",
      alignItems: "flex-start",
      margin: 10,
      borderWidth: 1,
      borderBottomEndRadius: 43,
      borderTopStartRadius: 43,
      minHeight: 160,
    },
    title: {
      margin: 10,
      fontSize: 34,
      fontWeight: "300",
      color: primaryColor,
    },
    like: {
      backgroundColor: noirCassé,
      width: "100%",
      alignItems: "center",
      borderBottomEndRadius: 29,
      borderTopStartRadius: 29,
      zIndex: -9,
    },
    plus: {
      tintColor: primaryColor,
      width: 70,
      height: 70,
      position: "absolute",
      bottom: 2,
      left: 323,
    },
  },
  modalMenu: {
    container: {
      height: 60,
      width: 122,
      borderWidth: 0.4,
      position: "absolute",
      top: -3,
      left: 237,
      backgroundColor: "white",
      shadowColor: "#000",
      shadowOffset: {
        width: 1,
        height: 5,
      },
      shadowOpacity: 0.27,
      shadowRadius: 6,
      elevation: 6,
      borderBottomLeftRadius: 9,
      borderBottomRightRadius: 9,
      borderTopRightRadius: 9,
      borderTopLeftRadius: 9,
      justifyContent: "flex-start",
    },
    menuItem: {
      margin: 4,
      backgroundColor: "whitesmoke",
      marginBottom: 2,
    },
  },
  verticalEvent: {
    container: {
      flex: 1,
      alignSelf: "center",
      borderRadius: 20,
      backgroundColor: "transparent",
      shadowColor: "black",
      shadowOffset: {
        width: 3,
        height: 6,
      },
      shadowOpacity: 0.45,
      shadowRadius: 4,
      elevation: 5,
      borderWidth: 0.1,
      marginBottom: 4,
      backgroundColor: "white",
      width: 370,
      padding: 6,
      justifyContent: "center",
      alignItems: "flex-start",
      margin: 10,
    },

    image: {
      height: 30,
      width: 30,
      tintColor: noirCassé,
    },
    field: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "flex-start",
      margin: 10,
    },
    icon: {
      height: 15,
      width: 15,
      marginLeft: 3,
      tintColor: noirCassé,
    },
    dateEvent: {
      creation: {
        fontStyle: "italic",
      },
      begin: {
        fontWeight: "200",
        fontSize: 18,
      },
    },
  },
  centeredView: {
    justifyContent: "center",
    height: "20%",
    position: "absolute",
    alignSelf: "center",
    bottom: 52,
    width: "97%",
    zIndex: 99,
  },
  modalView: {
    margin: 10,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 25,
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.25,
    shadowRadius: 5,
    elevation: 5,
  },

  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 10,
    textAlign: "center",
    fontWeight: "500",
    fontSize: 16,
  },
  image: {
    position: "absolute",
    left: 5,
  },
  invitations : {
    container : {
      backgroundColor : 'white',
      height: Dimensions.get("window").height,
    },
    searchBlock : {
      justifyContent : 'center',
      width : '90%',
      alignSelf : 'flex-end',
      paddingLeft : 20,
      
    }
  }
});
