import { View, FlatList } from 'react-native'
import React, { useEffect } from 'react'
import { useState } from 'react';
import ParticipantItem from './ParticipantItem';

const ParticipantsListe = ({ route }) => {

    const [participants, setParticipants] = useState([]);

    useEffect(() => {
        setParticipants(route.params?.participants);
    }, [route]);

    return (
        <View style={{ flex: 74, backgroundColor : 'white' }}>
           
            <FlatList
                style={{ marginBottom: 1 }}
                data={participants}
                renderItem={({ item }) => {
                    return <ParticipantItem participant={item} />
                }}
            />
        </View>
    )
}

export default ParticipantsListe