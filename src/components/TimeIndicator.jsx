import { View, Text } from "react-native";
import React, { useEffect, useState } from "react";
import { styles } from "../style/styles";
import { creationTime, dateDiff } from "../util/utils";

const TimeIndicator = ({ creation_date }) => {
  const [time, setTime] = useState(
    dateDiff(new Date(creation_date), new Date(Date.now()))
  );
  const [creationTimeLabel, setCreationTimeLabel] = useState(
    creationTime(time)
  );

  //const now = new Date(Date.now());
  //const [firstTime, setFirstTime] = useState(true);

  useEffect(() => {
    updateCreationTime();

    const interval = setInterval(updateCreationTime, 60000);

    // Nettoyer l'intervalle lorsque le composant est démonté
    return () => {
      clearInterval(interval);
    };
  }, []);

  const updateCreationTime = () => {
    setTime(dateDiff(new Date(creation_date), new Date(Date.now())));
    setCreationTimeLabel(creationTime(time));
  };


  return (
    <View style={[styles.timeIndicator.container]}>
      <Text style={styles.event_component.dateCreation}>
        {" "}
        {creationTimeLabel}{" "}
      </Text>
    </View>
  );
};

export default TimeIndicator;
