import { View, Text, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { styles } from '../style/styles'
import { NextIcon } from '../../assets/icon'

const NextItem = ({action}) => {

    
  return (
    <TouchableOpacity onPress={action}>
        <View style={[styles.myEvent.seeMore]}>
          <Image style={styles.myEvent.seeMore.icon} source={NextIcon} />
        </View>
      </TouchableOpacity>
  )
}

export default NextItem