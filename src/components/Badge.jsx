import { View, Text } from 'react-native'
import React from 'react'
import { styles } from '../style/styles'

const Badge = ({number}) => {
  return (
    <View style={[styles.badge]}>
      <Text style={[styles.text, {color : 'whitesmoke'}]}> {number} </Text>
    </View>
  )
}

export default Badge