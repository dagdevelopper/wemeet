import { View, Text, Image, ActivityIndicator, TouchableOpacity } from 'react-native'
import React from 'react'
import { noirCassé, styles } from '../style/styles'
import { EventIcon, EventUnknown, LocationIcon } from '../../assets/icon'
import { useState } from 'react'
import { useEffect } from 'react'
import ModalContainer from './ModalContainer'
import EventDetailsStack from '../navigation/EventDetailsStack'
import { dateToString } from '../util/utils'
import TimeIndicator from './TimeIndicator'

const Event = (props) => {
  const [event, setEvent] = useState(props.eventProp);
  const [open, setOpen] = useState(false);
  

  useEffect(() => {
    setEvent(props.eventProp);

  }, [props.eventProp]);

  const handleClick = () => {
    setOpen(true);
  }

  return (
    event ? (
      <TouchableOpacity onPress={handleClick} style={[styles.event_component.container]}>
        <View >
          <View style={[styles.field, { justifyContent: 'space-between' }]}>
            <View style={{ flexDirection: 'row' }}>
              <Image style={styles.icon} source={EventUnknown} />
              <Text style={styles.event_component.label}> {event.label} </Text>
            </View>
            <TimeIndicator creation_date={event.creation_date} />
            
          </View>

          <View style={[styles.field, { justifyContent: 'flex-start' }]}>
            <Image style={styles.icon} source={EventIcon} />
            <Text style={[styles.defaultText, {color : noirCassé}]}> {`${dateToString(new Date(event.date_begin))}  ${event.date_begin !== event.date_end ? ` - ${dateToString(new Date(event.date_end))}` : ''}`} </Text>
          </View>

          <View style={[styles.field, { justifyContent: 'flex-start' }]}>
            <Image style={styles.icon} source={LocationIcon} />
            <Text style={styles.event_component.city}> {event.city_name} </Text>
          </View>
          <Text style={styles.event_component.description}> {event.description} </Text>
        </View>


        <ModalContainer
          setShow={setOpen}
          show={open}
        >
          <EventDetailsStack event={event} />
        </ModalContainer>
      </TouchableOpacity>
    ) : (
      <ActivityIndicator />
    )
  )
}

export default Event