import { View, Text, Image } from 'react-native';
import React from 'react';
import { blue, primaryColor, styles } from '../style/styles';
import { ProfileIcon, VerifiedBadgeIcon } from '../../assets/icon';

const ParticipantItem = ({ participant }) => {
    
    return (
        <View style={[styles.participants.item]}>
            <View >
                <Image source={ProfileIcon} style={styles.profile.header.image} />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '70%' }}>
                <View>
                    <View style={{ flexDirection: 'row'}}>
                        <Text style={[styles.subTitle, { color: 'black' }]}> {participant?.first_name} {participant?.last_name} </Text>
                        {participant.is_admin && <Image source={VerifiedBadgeIcon} style={[styles.icon, { tintColor: blue, width : 16, height : 16}]} />}
                    </View>
                    <Text style={[styles.defaultText, { color: 'black' }]}> {participant?.email} </Text>
                </View>
                {participant.role === 'organizer' && (
                    <Text style={{ color: blue, fontStyle: 'italic' }}>Créateur</Text>
                )}

            </View>
        </View>
    )
}

export default ParticipantItem;