import { View, Text, Image } from 'react-native'
import React, { useState, useEffect } from 'react'
import { styles } from '../style/styles'
import { EventIcon, EventUnknown, LocationIcon, ScheduleIcon } from '../../assets/icon'
import { creationTime, dateDiff, dateToString } from '../util/utils'
import ModalContainer from './ModalContainer'
import EventDetailsStack from '../navigation/EventDetailsStack'
import { TouchableOpacity } from 'react-native-gesture-handler'

const VerticalEventItem = ({ event }) => {
    const [creationTimeLabel, setCreationTimeLabel] = useState('');
    const [time, setTime] = useState(dateDiff(new Date(event.creation_date), new Date(Date.now())));
    const [open, setOpen] = useState(false);


    useEffect(() => {
        setTime(dateDiff(new Date(event.creation_date), new Date(Date.now())));
        setCreationTimeLabel(creationTime(time));

    }, [time.min]);
    return (
        <TouchableOpacity onPress={() => setOpen(!open)} style={[styles.verticalEvent.container]}>

            <View style={styles.verticalEvent.field}>
                <Image style={styles.verticalEvent.image} source={EventUnknown} />
                <Text style={styles.event_component.city}>{event.label}</Text>
            </View>
            <View style={styles.verticalEvent.field}>
                <Image style={styles.verticalEvent.icon} source={ScheduleIcon} />
                <Text style={styles.verticalEvent.dateEvent.creation}> {creationTimeLabel} </Text>
            </View>
            <View style={styles.verticalEvent.field}>
                <Image style={styles.verticalEvent.image} source={EventIcon} />
                <Text style={styles.verticalEvent.dateEvent.begin}>
                    {`${dateToString(new Date(event.date_begin))}  ${event.date_begin !== event.date_end ? ` - ${dateToString(new Date(event.date_end))}` : ''}`}
                </Text>
            </View>
            <View style={styles.verticalEvent.field}>
                <Image style={styles.verticalEvent.image} source={LocationIcon} />
                <Text style={styles.event_component.city}> {event.city_name} </Text>
            </View>
            <Text style={styles.event_component.description}> {event.description} </Text>

            <ModalContainer
                setShow={setOpen}
                show={open}
            >
                <EventDetailsStack event={event} />
            </ModalContainer>

        </TouchableOpacity>
    )
}

export default VerticalEventItem