import { View, Text, Image, TouchableOpacity, Pressable, Alert } from 'react-native';
import React, { useContext, useState } from 'react';
import { noirCassé, orangeClair, rougeClair, styles, vert } from '../style/styles';
import { DoneIcon, ErrorIcon, LikeIcon, MoreIcon } from '../../assets/icon';
import { useLike, useSurveyLine } from '../ApiCalls/CustomHooks';
import { useDispatch } from 'react-redux';
import { deleteSurveyLineState, updateSurveyLineState } from '../redux_wemeet/Slices/survey.slice';
import FadeInView from './FadeInView';
import { useNavigation } from '@react-navigation/native';
import authProvider from '../auth/authProvider';
import PopupModal from './PopupModal';


const SurveyLineItem = ({ data, role }) => {

    const { updateSurveyLine, deleteSurveyLine } = useSurveyLine();
    const { like, unLike } = useLike();
    const dispatch = useDispatch();
    const [openMenu, setOpenMenu] = useState(false);
    const navigation = useNavigation();
    const { userConnected } = useContext(authProvider);
    const [showError, setShowError] = useState(false);
    const [error, setError] = useState('');
    const [show, setShow] = useState(false);
    const [msgModal, setMsgModal] = useState('');

    const handleLike = () => {

        like({ survey_line: data.line_number, person: userConnected?.id }).then(() => {
            updateSurveyLine({ label: data.label, nb_votes: data.nb_votes + 1, survey: data.survey, line_number: data.line_number, liked: true }).then(res => {
                dispatch(updateSurveyLineState(res.data));
            }).catch(e => {
                setShowError(!showError);
                setError(`Erreur au niveau du serveur : ${e.message}`);
                console.error("error update sl like : ", e);
            });
        }).catch(e => {
            setShowError(!showError);
            setError(`Erreur lors du like : ${e.message}`);
            console.error("error like : ", e);
        });


    }


    const handleDislike = () => {

        unLike({ survey_line: data.line_number, person: userConnected?.id }).then(() => {
            updateSurveyLine({ label: data.label, nb_votes: data.nb_votes - 1, survey: data.survey, line_number: data.line_number, liked: false }).then(res => {
                dispatch(updateSurveyLineState(res.data));
            }).catch(e => {
                setShowError(!showError);
                setError(`Erreur au niveau du serveur : ${e.message}`);
                console.error("error update sl disLike : ", e)
            });
        }).catch(e => {
            setShowError(!showError);
            setError(`Erreur lors du dislike : ${e.message}`);
            console.error("error unLike : ", e)
        });
    };

    const handleMenu = () => {
        setOpenMenu(!openMenu);
    }


    const handleDelete = () => {

        Alert.alert(
            `${data.label}`,
            'Supprimer la suggestion ?',
            [
                {
                    text: 'annuler',
                    style: 'cancel'
                },
                {
                    text: 'supprimer',
                    onPress: () => {
                        deleteSurveyLine({ line_number: data.line_number, survey: data.survey }).then(() => {
                            dispatch(deleteSurveyLineState({ line_number: data.line_number, survey: data.survey }));
                            setShow(!show);
                            setMsgModal('Suggestion supprimée ! ');
                        }).catch(e => {
                            setShowError(!showError);
                            setError(`Erreur de suppression de la suggestion : ${e.message}`);
                            console.error('error delete surveyLine : ', e)
                        });
                    }
                }
            ]
        );
    }

    const modalMenu = () => {
        return (
            <FadeInView duration={500}>
                <View style={[styles.modalMenu.container, { opacity: openMenu }]}>
                    <View>
                        <TouchableOpacity onPressOut={() => {
                            navigation.navigate('Gérer Suggestion', { survey: data.survey, toUpdate: true, label: data.label, line_number: data.line_number, nb_votes: data.nb_votes, liked: data.liked });
                            handleMenu();
                        }}>
                            <Text style={styles.modalMenu.menuItem}>modifier</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPressOut={handleDelete}>
                            <Text style={[{ color: 'red' }, styles.modalMenu.menuItem]}>supprimer</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </FadeInView>
        )
    }

    return (
        <>
            {
                data.label && (
                    <View style={[styles.surveyItem.container]}>

                        {role === 'organizer' &&
                            <>
                                <Pressable onPressOut={handleMenu} style={{ alignSelf: 'flex-end', marginRight: 12 }}>
                                    <Image source={MoreIcon} />
                                </Pressable>
                                {modalMenu()}
                            </>
                        }

                        <Text style={[styles.defaultText, { color: 'black', fontSize: 30 }]}> {data.label} </Text>
                        <Text style={{ color: 'gray', fontStyle: 'italic', paddingBottom: 12, }}> {data.nb_votes > 0 ? `${data.nb_votes} Personne${data.nb_votes > 1 ? 's' : ''} ${data.nb_votes > 1 ? 'ont' : 'a'} voté !` : 'Soyez la première personne à voter !'} </Text>
                        <TouchableOpacity style={[styles.surveyItem.like, { backgroundColor: data.liked ? orangeClair : noirCassé }]} onPressOut={() => {
                            if (data.liked)
                                handleDislike();
                            else
                                handleLike();
                        }}>
                            <Image style={{ tintColor: data.liked ? noirCassé : 'whitesmoke', width: 40, height: 40 }} source={LikeIcon} />
                        </TouchableOpacity>

                        <PopupModal
                            message={msgModal}
                            bgColor={orangeClair}
                            icon={DoneIcon}
                            colorIcon={vert}
                            show={show}
                            setShow={setShow}
                        />
                        <PopupModal
                            message={error}
                            bgColor={rougeClair}
                            icon={ErrorIcon}
                            colorIcon={noirCassé}
                            show={showError}
                            setShow={setShowError}
                        />
                    </View>
                )
            }
        </>
    )

}

export default SurveyLineItem