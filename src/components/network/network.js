import NetInfo from '@react-native-community/netinfo';


export const checkNetwork = async () => {
    const res = await NetInfo.fetch();
    return res.isConnected;
}
