import { View, Text, Pressable, Image } from 'react-native';
import React from 'react';
import { VisibleIcon, UnvisibleIcon } from '../../assets/icon';
import { useState } from 'react';
import { noirCassé, styles } from '../style/styles';

const HidePassword = ({ secureText=true, setSecureText, style }) => {
    const [hide, setHide] = useState(secureText);


    const handleHide = () => {
        setSecureText && setSecureText(!secureText);
        
    }
    return (
        <View style={style ? style : styles.eyes}>
            <Pressable onPress={handleHide}>
                <Image source={secureText ? VisibleIcon : UnvisibleIcon} style={[styles.icon, {tintColor : noirCassé, width : 22, height : 22}]} />
            </Pressable>
        </View>
    )
}

export default HidePassword