import { Animated } from 'react-native';
import React, { useRef, useEffect } from 'react';

const FadeInView = ({ children, duration = 3000, style }) => {
    const fadeAnim = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        Animated.timing(
            fadeAnim,
            {toValue : 1, duration : duration, useNativeDriver : true}
        ).start();
    }, [fadeAnim]);
    return (
        <Animated.View style={{...style, opacity : fadeAnim}}>
            {children}
        </Animated.View>
            
    )

}

export default FadeInView;