import { View, Text, Image, ImageBackground, Button, TouchableOpacity, ActivityIndicator } from 'react-native'
import React, { useContext } from 'react'
import { LogoIcon, WemeetIcon } from '../../assets/icon'
import { styles } from '../style/styles'
import FadeInView from './FadeInView'
import { useEffect } from 'react'
import { wait } from '../util/utils'
import { useState } from 'react'
import authProvider from '../auth/authProvider'
import { hasAuthentication } from '../auth/authentication'
import { useEventRole, useInvitation, useUser } from '../ApiCalls/CustomHooks'
import { setinvitations } from '../redux_wemeet/Slices/invitation.slice'
import { useDispatch } from 'react-redux'


const WelcomePage = ({ navigation }) => {
    const [backgroundLoading, setBackgroundLoading] = useState(false);
    const [show, setShow] = useState(false);
    const { getUser } = useUser();
    const { getMyEvents } = useEventRole();
    const {getInvitations} = useInvitation();
    const dispatch = useDispatch();
    const { setIsAuthenticated, setUserConnected, setOrganized, setParticipated } = useContext(authProvider);

    useEffect(() => {
        show && wait(6000).then(() => {

            hasAuthentication().then(res => {
                if (res.isAuthneticated) {
                    setIsAuthenticated(true);
                    getUser(res.userDatas.id).then(user => {
                        setUserConnected(user);

                        getMyEvents(user.id).then(res => {
                            const { participated, organized } = res.data;
                            setOrganized(organized.length);
                            setParticipated(participated.length);

                            navigation.navigate('Home');
                    
                        }).catch(e => {
                            console.log("myEvent : ", e);
                            setIsAuthenticated(false);
                            navigation.navigate('Login');
                            
                        });

                        getInvitations({email : user.email}).then(res => {
                            dispatch(setinvitations(res.data));
                        }).catch(e => {
                            console.log("invitation loading error : ", e);
                            setIsAuthenticated(false);
                            navigation.navigate('Login');
                            
                        });

                    }).catch(e => {
                        console.log("error user : ", e);
                        setIsAuthenticated(false);
                        navigation.navigate('Login');
                        
                    });


                } else {

                    navigation.navigate('Login');
                    setIsAuthenticated(false);
                }
            }).catch(e => {
                console.log("error token : ", e.message);
                setIsAuthenticated(false);
                navigation.navigate('Login');
            })

        }).catch((e) => console.log("error wait wlc : ", e))
    }, [show]);

    


    return (
        <ImageBackground onLoadStart={() => setBackgroundLoading(!backgroundLoading)} onLoad={() => {
            setBackgroundLoading(!backgroundLoading);
            wait(1000).then(() => setShow(true))
        }} resizeMethod='auto' source={WemeetIcon} blurRadius={4} style={[styles.container, { backgroundColor: 'black', height: "100%" }]}
        >
            {backgroundLoading ? (
                <ActivityIndicator color={'orange'} accessibilityLabel='Chargement' size={'large'} />
            ) : (
                show && (
                    <FadeInView duration={3000} >
                        <View style={styles.logoPage.container}>
                            <Image style={[styles.logo.image, styles.logoPage.logo]} source={LogoIcon} />
                            <Text style={[styles.defaultText, styles.shadow, styles.logoPage.text]}>emeet</Text>
                        </View>
                    </FadeInView>
                )
            )}
        </ImageBackground>
    )

}

export default WelcomePage