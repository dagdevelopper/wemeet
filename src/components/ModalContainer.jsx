import { Modal } from 'react-native'
import React from 'react'

const ModalContainer = ({ transparent = false, children, show = false, setShow, animationType = 'slide', style, presentationStyle ='pageSheet' }) => {
    return (
        <Modal visible={show}
            transparent={transparent}
            style={style}
            onRequestClose={() => setShow(false)}
            animationType={animationType}
            presentationStyle={presentationStyle}
            
        >
            {children}
        </Modal>
    )
}


export default ModalContainer