import { Alert, Modal, StyleSheet, Text, Pressable, View, Image } from "react-native";

import React, { useState, useEffect } from 'react'
import { CloseIcon } from "../../assets/icon";
import FadeInView from "./FadeInView";
import { noirCassé, styles } from "../style/styles";
import { wait } from "../util/utils";
import ModalContainer from "./ModalContainer";

const PopupModal = ({ show, setShow, message, bgColor, colorIcon, colorMsg, icon, animationType }) => {
    const [modalVisible, setModalVisible] = useState(false);

    useEffect(() => {
        setModalVisible(show);
        show && wait(2000).then(() => {
            setShow(false);
        })

    }, [show]);

    return (

        <ModalContainer animationType={animationType} presentationStyle='overFullScreen' transparent={true} show={show} setShow={setShow}>
            <View style={[styles.centeredView]}>
                <FadeInView duration={1000} >
                    <View >
                        <View style={[styles.modalView, { backgroundColor: bgColor ?? 'white' }]}>
                            <Pressable style={styles.image} onPress={() => setShow(!modalVisible)}>
                                <Image style={{ width: 30, height: 30, tintColor: colorMsg ?? 'black' }} source={CloseIcon} />
                            </Pressable>
                            <View style={[styles.field, {alignItems : 'center', justifyContent : 'space-around'}]}>
                                <Text style={[styles.modalText, { color: colorMsg ?? 'black' }]}> {message ?? 'Put a message !'} </Text>
                                {icon && <Image style={{ width: 30, height: 30, tintColor: colorIcon ?? noirCassé }} source={icon} />}
                            </View>
                        </View>
                    </View>
                </FadeInView>
            </View>
        </ModalContainer>
    );
}

export default PopupModal