import { View, Text, Image, TouchableOpacity } from "react-native";
import React from "react";
import { styles } from "../style/styles";
import { MenuIcon } from "../../assets/icon";
import SearchBarApp from "./SearchBarApp";

const TabBar = ({ title, setSearchCity, searchCity }) => {
  return (
    <View style={styles.menuBar.container}>
      <View style={[styles.menuBar.elements]}>
        <Image
          source={MenuIcon}
          style={[styles.icon, { tintColor: "whitesmoke" }]}
        />

        <Text
          style={{
            fontSize: 20,
            fontWeight: "600",
            color: "whitesmoke",
            marginLeft: 6,
          }}
        >
          {title}
        </Text>
      </View>
      <View>
        <SearchBarApp setSearchCity={setSearchCity} searchCity={searchCity} />
      </View>
    </View>
  );
};

export default TabBar;
