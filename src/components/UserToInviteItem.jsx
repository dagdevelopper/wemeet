import { View, Text } from "react-native";
import React, { useContext, useEffect, useState } from "react";
import { useInvitation } from "../ApiCalls/CustomHooks";
import { blue, primaryColor, styles } from "../style/styles";
import { Image } from "react-native";
import { VerifiedBadgeIcon } from "../../assets/icon";
import { TouchableOpacity } from "react-native";
import { ActivityIndicator } from "react-native";
import authProvider from "../auth/authProvider";

const UserToInviteItem = ({ user, alreadyInvited, invitations, eventId }) => {
  const { createInvitation, deleteInvitation } = useInvitation();
  const [loading, setLoading] = useState(false);
  const [invited, setInvited] = useState(false);
  const { userConnected } = useContext(authProvider);

  useEffect(() => {
    setInvited(inviteNotAcceptedFound(invitations));
  }, [invitations]);

  const inviteNotAcceptedFound = (invitations) => {
    const invitation = invitations?.find(
      (i) =>
        i.sender === userConnected?.email &&
        i.guest === user?.email &&
        i.accepted === false
    );

    return invitation !== undefined;
  };

  const handleInvite = () => {
    setLoading(true);

    if (!invited) {
      createInvitation({
        userId: userConnected?.id,
        eventId,
        guest: user?.email,
      })
        .then(() => {
          
          setLoading(false);
          setInvited(!invited);
          
        })
        .catch((e) => {
          console.log("error create invitation : ", e);
          setLoading(false);
        });
    } else {
      deleteInvitation({ guest: user?.email, eventId })
        .then(() => {
          
          setLoading(false);
          setInvited(!invited);
        })
        .catch((e) => {
          console.log("error delete invitation : ", e);
          setLoading(false);
        });
    }
  };

  return (
    <View style={[styles.participants.item]}>
      {user && (
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            width: "100%",
            padding: 7,
            marginBottom: 2,
            marginTop: 10,
          }}
        >
          <View>
            <View style={{ flexDirection: "row" }}>
              <Text style={[styles.subTitle, { color: "black" }]}>
                {" "}
                {user?.first_name} {user?.last_name}{" "}
              </Text>
              {user?.is_admin && (
                <Image
                  source={VerifiedBadgeIcon}
                  style={[
                    styles.icon,
                    { tintColor: blue, width: 16, height: 16 },
                  ]}
                />
              )}
            </View>
            <Text style={[styles.defaultText, { color: "black" }]}>
              {" "}
              {user?.email}{" "}
            </Text>
          </View>
          {alreadyInvited ? (
            <Text style={[styles.subTitle, { color: blue }]}>Invité(e)</Text>
          ) : (
            <>
              {!invited && (
                <TouchableOpacity
                  style={{
                    backgroundColor: blue,
                    width: "20%",
                    justifyContent: "center",
                    borderRadius: 3,
                  }}
                  onPressOut={handleInvite}
                >
                  {!loading && (
                    <Text
                      style={[
                        styles.subTitle,
                        { color: "white", textAlign: "center" },
                      ]}
                    >
                      Inviter
                    </Text>
                  )}
                  {loading && <ActivityIndicator color={"white"} />}
                </TouchableOpacity>
              )}
              {invited && (
                <TouchableOpacity onPressOut={handleInvite}>
                  {!loading && (
                    <Text style={[styles.subTitle, { color: primaryColor }]}>
                      Annuler
                    </Text>
                  )}
                  {loading && (
                    <ActivityIndicator
                      color={primaryColor}
                      style={{
                        alignSelf: "flex-start",
                        justifyContent: "center",
                        width: 80,
                        flex: 1,
                      }}
                    />

                  )}
                </TouchableOpacity>
              )}
            </>
          )}
        </View>
      )}
    </View>
  );
};

export default UserToInviteItem;
