import { View, ActivityIndicator, FlatList, Text, RefreshControl } from 'react-native'
import React from 'react'
import Event from './Event';
import { styles } from '../style/styles';
import { useState, useCallback } from 'react';
import { wait } from '../util/utils';




const EventListe = ({ events, emptyList_fct,
  isVerticalList = true, showVerticalScroll = true,
  showHorizontalScroll = false, showPerPage = false, separatorComponent,
  lastItem, setRefreshControl = false, onRefreshAction, timeOutOnRefresh = 2000 }) => {


  const [refreshing, setRefreshing] = useState(false);


  const onRefresh = useCallback(() => {
    setRefreshing(true);
    onRefreshAction();
    wait(timeOutOnRefresh).then(() => setRefreshing(false));
  }, [timeOutOnRefresh]);



  return (

    <View style={[styles.eventListe.container]}>

      <FlatList

        ListEmptyComponent={emptyList_fct}
        ItemSeparatorComponent={separatorComponent}
        horizontal={!isVerticalList}
        pagingEnabled={showPerPage}
        showsVerticalScrollIndicator={showVerticalScroll}
        showsHorizontalScrollIndicator={showHorizontalScroll}
        data={events}
        refreshControl={
          setRefreshControl ? <RefreshControl refreshing={refreshing} onRefresh={onRefresh} /> : undefined
        }

        ListFooterComponent={() => {
          return lastItem ? lastItem : undefined;
        }}
        renderItem={({ item }) => {
          return <Event eventProp={item} />
        }}

      />
    </View>
  )
}

export default EventListe;