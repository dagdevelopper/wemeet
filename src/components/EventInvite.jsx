import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  TouchableHighlight,
} from "react-native";
import React from "react";
import {
  blue,
  noirCassé,
  orangeClair,
  rougeClair,
  styles,
} from "../style/styles";
import { creationTime, dateDiff, dateToString } from "../util/utils";
import {
  EventIcon,
  EventUnknown,
  LocationIcon,
  ScheduleIcon,
} from "../../assets/icon";
import { useState } from "react";
import { useEffect } from "react";
import { useNavigation, useTheme } from "@react-navigation/native";
import { Alert } from "react-native";
import { useEventRole, useInvitation } from "../ApiCalls/CustomHooks";
import { useContext } from "react";
import authProvider from "../auth/authProvider";
import { useDispatch } from "react-redux";
import {
  addParticipated,
  removeParticipated,
  subscribe,
  unSubscribe,
} from "../redux_wemeet/Slices/event.slice";
import {
  deleteInvite,
  updateInvite,
} from "../redux_wemeet/Slices/invitation.slice";


const EventInvite = ({ event, accepted }) => {
  const [creationTimeLabel, setCreationTimeLabel] = useState("");
  const [time, setTime] = useState(
    dateDiff(new Date(parseInt(event?.creation_date)), new Date(Date.now()))
  );
  const navigation = useNavigation();
  const [inviteAccepted, setAccepted] = useState(accepted);
  const { updateInvitation, deleteInvitation } = useInvitation();
  const { userConnected, setParticipated, nbEventsParticipated } =
    useContext(authProvider);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const { subscribeParticipant, unSubscribeParticipant } = useEventRole();
  const [isDelete, SetIsDelete] = useState(false);


  useEffect(() => {
    setAccepted(accepted);
    setTime(
      dateDiff(new Date(parseInt(event?.creation_date)), new Date(Date.now()))
    );
    setCreationTimeLabel(creationTime(time));

  }, [accepted, event]);

  const handleAcceptInvite = () => {
    setLoading(true);
    updateInvitation({
      accepted: !inviteAccepted,
      eventId: event.id,
      guest: userConnected.email,
    })
      .then(() => {
        setAccepted(!inviteAccepted);
        dispatch(
          updateInvite({ eventId: event.id, accepted: !inviteAccepted })
        );

        if (inviteAccepted) {
          unSubscribeParticipant({
            userId: userConnected.id,
            eventId: event.id,
          })
            .then((res) => {
              dispatch(unSubscribe(res.data));
              dispatch(removeParticipated(event.id));
              setParticipated(nbEventsParticipated - 1);
              setLoading(false);
            })
            .catch((e) => {
              console.log("error subscribe : ", e);
              setLoading(false);
            });
        } else {
          subscribeParticipant({ userId: userConnected.id, eventId: event.id })
            .then((res) => {
              dispatch(subscribe(res.data));
              dispatch(addParticipated(event));
              setParticipated(nbEventsParticipated + 1);
              setLoading(false);
            })
            .catch((e) => {
              console.log("error subscribe : ", e);
              setLoading(false);
            });
        }

        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        console.error(e);
      });
  };

  const handleDeclineInvite = () => {
    Alert.alert(`${event.label}`, "Décliner l'invitation ?", [
      {
        text: "annuler",
        style: "cancel",
      },
      {
        text: "Décliner",
        onPress: () => {
          setLoading(true);
          deleteInvitation({ guest: userConnected?.email, eventId: event?.id })
            .then(() => {
              setLoading(false);
              dispatch(deleteInvite({ eventId: event?.id }));
              SetIsDelete(true);
            })
            .catch((e) => {
              console.log("error delete invite : ", e);
              setLoading(false);
            });
        },
      },
    ]);
  };

  return event ? (
    <View style={[styles.event_component.container, { width: 400 }]}>
      <TouchableOpacity
        onPress={() => console.log("click ")}
      >
        <View
          style={[
            styles.field,
            { justifyContent: "space-between", width: "83%" },
          ]}
        >
          <View style={{ flexDirection: "row" }}>
            <Image style={styles.icon} source={EventUnknown} />
            <Text style={styles.event_component.label}> {event.label} </Text>
          </View>
        </View>

        <View style={[styles.field, { justifyContent: "flex-start" }]}>
          <Image
            style={[styles.verticalEvent.icon, { tintColor: orangeClair }]}
            source={ScheduleIcon}
          />
          <Text style={styles.verticalEvent.dateEvent.creation}>
            {" "}
            {creationTimeLabel}{" "}
          </Text>
        </View>

        <View style={[styles.field, { justifyContent: "flex-start" }]}>
          <Image style={styles.icon} source={EventIcon} />
          <Text style={[styles.defaultText, { color: noirCassé }]}>
            {" "}
            {`${dateToString(new Date(event.date_begin))}  ${
              event.date_begin !== event.date_end
                ? ` - ${dateToString(new Date(event.date_end))}`
                : ""
            }`}{" "}
          </Text>
        </View>

        <View style={[styles.field, { justifyContent: "flex-start" }]}>
          <Image style={styles.icon} source={LocationIcon} />
          <Text style={styles.event_component.city}> {event.city_name} </Text>
        </View>

        <Text
          style={[styles.event_component.description, { color: noirCassé }]}
        >
          {" "}
          {event.description}{" "}
        </Text>
      </TouchableOpacity>

      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          width: "100%",
          padding: 12,
        }}
      >
        <TouchableOpacity
          disabled={inviteAccepted || isDelete}
          onPressOut={() => {
            handleDeclineInvite();
          }}
          style={[
            styles.defaultRoundedBorder,
            { backgroundColor: inviteAccepted ? "darkgray" : noirCassé },
          ]}
        >
          {!loading && (
            <Text
              style={[
                styles.text,
                { color: inviteAccepted ? "black" : "whitesmoke", padding: 7 },
              ]}
            >
              {`${isDelete ? "Invitation supprimée" : "Décliner"}`}
            </Text>
          )}
          {loading && (
            <ActivityIndicator
              color={"white"}
              style={[{ padding: 7, width: 95 }]}
            />
          )}
        </TouchableOpacity>

        {!isDelete && (
          <TouchableOpacity
            disabled={isDelete}
            onPressOut={() => {
              handleAcceptInvite();
            }}
            style={[
              styles.defaultRoundedBorder,
              {
                backgroundColor: inviteAccepted || isDelete ? rougeClair : blue,
              },
            ]}
          >
            {!loading && (
              <Text style={[styles.text, { color: "whitesmoke", padding: 7 }]}>
                {" "}
                {loading ? (
                  <ActivityIndicator />
                ) : inviteAccepted ? (
                  "Ne plus accepter"
                ) : (
                  "Accepter"
                )}{" "}
              </Text>
            )}
            {loading && (
              <ActivityIndicator
                color={"white"}
                style={[{ padding: 7, width: 95 }]}
              />
            )}
          </TouchableOpacity>
        )}
      </View>
    </View>
  ) : (
    <ActivityIndicator />
  );
};

export default EventInvite;
