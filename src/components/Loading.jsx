import { ActivityIndicator } from 'react-native'
import React, { useContext } from 'react'
import { useEffect } from 'react'
import { styles } from '../style/styles';
import Auth from '../auth/authProvider';

const Loading = ({ navigation }) => {
  const { isAuthenticated } = useContext(Auth);

  useEffect(() => {

    isAuthenticated ? navigation.navigate('Home') : navigation.navigate('Login');
    
  }, [isAuthenticated]);

  return (
    <ActivityIndicator style={styles.container} color={'orange'} accessibilityLabel='Chargement' size={'large'} />
  )
}

export default Loading