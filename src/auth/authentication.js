import axios from 'axios';
import { REACT_APP_API_URL, TOKEN_NAME } from '@env';
import jwtDecode from 'jwt-decode';
import { getItem, removeItem, setItem } from './AsyncStorage';

export const logout = async () => {
    await removeItem(TOKEN_NAME);
}

export const hasAuthentication = async () => {

    let result = { isAuthneticated: false, userDatas: {} };

    const token = await getItem(TOKEN_NAME);
    const { exp, value } = jwtDecode(token);
    const tokenIsValid = exp * 1000 > new Date().getTime();

    result.isAuthneticated = token && tokenIsValid;
    result.userDatas = value;

    !tokenIsValid && removeItem(TOKEN_NAME).then(() => result.userDatas = {});
    return result;

}


export const login = async ({ email, password }) => {
    
    const res = await axios.post(`${REACT_APP_API_URL}/user/login`, { email, password });
    await setItem({ tokenName: TOKEN_NAME, token: res.data });
    const { value } = jwtDecode(res.data);
    return value;

}
