import AsyncStorage from '@react-native-async-storage/async-storage';

export const getItem = async (tokenName) => {
    const token = await AsyncStorage.getItem(tokenName);
    return token;
}

export const setItem = async ({ tokenName, token }) => {
    await AsyncStorage.setItem(tokenName, token);
}

export const removeItem = async (tokenName) => {
    await AsyncStorage.removeItem(tokenName);
}