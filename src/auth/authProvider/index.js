import { createContext } from "react";

export default createContext({
    isAuthenticated: false,
    setIsAuthenticated: value => { },
    userConnected: null,
    setUserConnected: value => { },
    nbEventsParticipated: 0,
    nbEventsOrganized: 0,
    setParticipated: value => { },
    setOrganized: value => { }
});