
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import WelcomePage from '../components/WelcomePage';
import Loading from '../components/Loading';
import ConnectionStack from './ConnectionStack';
import EventDetailsStack from './EventDetailsStack';
import MenuBottom from './MenuBottom';

const RootStack = createNativeStackNavigator();

const Root = () => {
    return (
        <RootStack.Navigator>
            <RootStack.Group screenOptions={{headerShown : false, gestureEnabled : false, animation : 'fade', animationDuration : 500, animationTypeForReplace : 'pop'}}>
                <RootStack.Screen
                    name='welcome'
                    component={WelcomePage}
                />
                <RootStack.Screen
                    name='Loading'
                    component={Loading}
                />
                <RootStack.Screen
                    name='Login'
                    component={ConnectionStack}
                />
               
                <RootStack.Screen
                    name='Home'
                    component={MenuBottom}
                />
            </RootStack.Group>

            <RootStack.Screen
                    name='eventDetails'
                    component={EventDetailsStack}
                />
        </RootStack.Navigator>
    )
}

export default Root;