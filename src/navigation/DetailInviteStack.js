import { View, Text } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import InvitationDetail from '../screens/InvitationDetail';
import { primaryColor } from '../style/styles';
import ParticipantsListe from '../components/ParticipantsListe';

const Stack = createNativeStackNavigator();

const DetailInviteStack = ({ route }) => {


    return (
        <Stack.Navigator >

            <Stack.Screen
                name='details'
                component={InvitationDetail}
                initialParams={{ event: route.params.event }}
                options={{ title: 'Détails', headerBackVisible: true, headerBackTitleVisible: false, animation: 'simple_push', headerTintColor: primaryColor, presentation: 'modal' }}
            />
            <Stack.Screen
                name='ParticipantsListe'
                component={ParticipantsListe}
                options={{ headerBackVisible: true, animation: 'simple_push', animationDuration: 500, headerBackTitleVisible: false, headerTintColor: primaryColor }}
            />

        </Stack.Navigator>
    )
}

export default DetailInviteStack