import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Profile from '../screens/Profile';
import Invitations from '../screens/Invitations';
import { primaryColor } from '../style/styles';
import InvitationDetail from '../screens/InvitationDetail';
import ParticipantsListe from '../components/ParticipantsListe';
import InvitationStack from './InvitationStack';

const Stack = createNativeStackNavigator();

const ProfileStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen 
                name='profile-screen'
                component={Profile}
                options={{ headerShown: false }}
            />
            <Stack.Screen 
                name='invitations'
                component={ InvitationStack }
                options={{ title: 'Invitations', headerBackVisible : true, headerBackTitleVisible : false, animation : 'simple_push', headerTintColor : primaryColor, presentation : 'modal' }}
            />
            
        </Stack.Navigator>

    )
}

export default ProfileStack;
