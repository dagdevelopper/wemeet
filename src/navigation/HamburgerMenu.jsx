import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import MenuBottom from './MenuBottom';
//import ConnectionStack from './ConnectionStack';

const Drawer = createDrawerNavigator();

/*const DrawerContent = (props) => {
    return (
        <View>
            <ImageBackground source={WemeetIcon}>
                <Text>Username</Text>
            </ImageBackground>
            <View>
                <DrawerItem {...props} />
            </View>
        </View>
    );
}*/

const options = {
    drawerActiveBackgroundColor : 'orange',
    drawerActiveTintColor : 'black',
    drawerStatusBarAnimation : 'fade',
    freezeOnBlur: true,


}

const HamburgerMenu = () => {
  return (
    <Drawer.Navigator  useLegacyImplementation ={true}>
        <Drawer.Screen name='HomeMenu' component={MenuBottom} options={{headerShown: false, ...options}} />
        
        
    </Drawer.Navigator>
  )
}
//navigation pour logout



export default HamburgerMenu;