import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Login from "../screens/Login";
import Registration from "../screens/Registration";
import HamburgerMenu from "./HamburgerMenu";

const Stack = createNativeStackNavigator();


const ConnectionStack = () => {
  return (

    <Stack.Navigator >
      <Stack.Screen
        name='Log-in'
        component={Login}
        options={{ title: 'Login', headerShown: false }}
      />
      <Stack.Screen
        name='Registration'
        component={Registration}
        options={{ title: 'Registration', headerShown: false}}

      />
      
      
    </Stack.Navigator>
  );
}

export default ConnectionStack;
