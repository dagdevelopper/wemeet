import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Invitations from "../screens/Invitations";
import InvitationDetail from "../screens/InvitationDetail";
import ParticipantsListe from "../components/ParticipantsListe";
import { primaryColor } from "../style/styles";
import DetailInviteStack from "./DetailInviteStack";

const Stack = createNativeStackNavigator();

const InvitationStack = ({invitations}) => {

    return (

        <Stack.Navigator >
          <Stack.Screen 
                name='invitationsListe'
                component={Invitations}
                initialParams={{invitations : invitations}}
                options={{ title: 'Invitations', headerBackVisible : true, headerBackTitleVisible : false, animation : 'simple_push', headerTintColor : primaryColor, presentation : 'modal' }}
            />
            <Stack.Screen 
                name='invitationsDetail'
                component={DetailInviteStack}
                options={{ headerShown : false , animation : 'simple_push', headerTintColor : primaryColor, presentation : 'modal' }}
            />
           
          
        </Stack.Navigator>
      );
}

export default InvitationStack;