import { View, Text } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import EventDetails from '../screens/EventDetails';
import ParticipantsListe from '../components/ParticipantsListe';
import { primaryColor } from '../style/styles';
import SurveyListe from '../screens/SurveyListe';
import NewSurvey from '../screens/NewSurvey';
import NewSurveyLine from '../screens/NewSurveyLine';
import InvitationScreen from '../screens/InvitationScreen';

const Stack = createNativeStackNavigator();

const EventDetailsStack = ({ event, DATA }) => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name='Détails'
                component={EventDetails}
                initialParams={{ event }}
                options={{ headerShown: false }}

            />
            <Stack.Screen 
                name='Sondages'
                component={SurveyListe}
                initialParams={{DATA}}
                options={{ headerBackVisible: true, animation: 'simple_push', animationDuration: 500, headerBackTitleVisible: false, headerTintColor : primaryColor }}
            />
            <Stack.Screen 
                name='Gérer Sondage'
                component={NewSurvey}
                options={{ headerBackVisible: true, animation: 'simple_push', animationDuration: 500, headerBackTitleVisible: false, headerTintColor : primaryColor }}
            />
            <Stack.Screen 
                name='Gérer Suggestion'
                component={NewSurveyLine}
                options={{ headerBackVisible: true, animation: 'simple_push', animationDuration: 500, headerBackTitleVisible: false, headerTintColor : primaryColor }}
            />
            <Stack.Screen
                name='Participants'
                component={ParticipantsListe}
                options={{ headerBackVisible: true, animation: 'simple_push', animationDuration: 500, headerBackTitleVisible: false, headerTintColor : primaryColor }}
            />
            <Stack.Screen
                name='Invitation'
                component={InvitationScreen}
                initialParams={{participants : [], event : {}}}
                options={{headerTitle : 'INVITEZ VOS AMIS(ES)', headerBackVisible: true, animation: 'simple_push', animationDuration: 500, headerBackTitleVisible: false, headerTintColor : primaryColor }}
            />
            
        </Stack.Navigator>
    )
}

export default EventDetailsStack;