import { useState } from "react";
import axiosRetry from "axios-retry";
import axios from "axios";
import { REACT_APP_API_URL, TOKEN_NAME } from "@env";
import { getItem } from "../auth/AsyncStorage";

axiosRetry(axios, {
  retries: 3,
  retryDelay: axiosRetry.exponentialDelay,
});

//                                      EVENT

const useEvent = () => {
  const [token, setToken] = useState("");

  getItem(TOKEN_NAME)
    .then((res) => {
      setToken(res);
    })
    .catch((e) => {
      console.log("token error custom hooks : ", e);
    });

  const getAllEvent = async ({ email }) => {
    const res = await axios({
      method: "get",
      url: `${REACT_APP_API_URL}/event/all/${email}`,
    });
    return res.data;
  };

  const newEvent = async ({
    label,
    description,
    location,
    isPrivate,
    email,
    dateBegin,
    dateEnd,
  }) => {
    return await axios({
      method: "post",
      url: `${REACT_APP_API_URL}/event/`,
      data: {
        label,
        description,
        cityName: location,
        isPrivate,
        email,
        dateBegin,
        dateEnd,
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  const removeEvent = async ({ id }) => {
    return await axios({
      method: "delete",
      url: `${REACT_APP_API_URL}/event/`,
      data: { id },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  return {
    getAllEvent,
    newEvent,
    removeEvent,
  };
};

//                          USER

const useUser = () => {
  const [token, setToken] = useState("");

  getItem(TOKEN_NAME)
    .then((res) => {
      setToken(res);
    })
    .catch((e) => {
      console.log("token error custom hooks : ", e);
    });

  const getUser = async (userId) => {
    const res = await axios.get(`${REACT_APP_API_URL}/user/${userId}`);
    return res.data;
  };

  const getUsers = async () => {
   
    try {
      const token = await getItem(TOKEN_NAME);
      const response = await axios({
        method: "get",
        url: `${REACT_APP_API_URL}/user/all`,
        headers: {
          Authorization: "Bearer " + token,
        },
      });

      return response.data;
    } catch (error) {
      console.log("Erreur get Users : ", error);
    }
  };

  const addUser = async ({
    email,
    firstName,
    lastName,
    address,
    postalCode,
    isAdmin,
    password,
  }) => {
    return await axios({
      method: "post",
      url: `${REACT_APP_API_URL}/user/`,
      data: {
        email,
        firstName,
        lastName,
        address,
        postalCode,
        isAdmin,
        password,
      },
    });
  };

  const updateUser = async ({
    email,
    firstName,
    lastName,
    address,
    postalCode,
    id,
    isAdmin,
  }) => {
    return await axios({
      method: "patch",
      url: `${REACT_APP_API_URL}/user/`,
      data: { email, firstName, lastName, address, postalCode, id, isAdmin },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  return {
    getUser,
    addUser,
    updateUser,
    getUsers,
  };
};

//                  EVENT ROLE

const useEventRole = () => {
  const [token, setToken] = useState("");

  getItem(TOKEN_NAME)
    .then((res) => {
      setToken(res);
    })
    .catch((e) => {
      console.log("token error custom hooks : ", e);
    });

  const getMyEvents = async (id) => {
    return await axios.get(`${REACT_APP_API_URL}/eventRole/roles/${id}`);
  };

  const subscribeParticipant = async ({ userId, eventId }) => {
    return await axios({
      method: "post",
      url: `${REACT_APP_API_URL}/eventRole/`,
      data: { userId, eventId },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  const unSubscribeParticipant = async ({ userId, eventId }) => {
    return await axios({
      method: "delete",
      url: `${REACT_APP_API_URL}/eventRole/`,
      data: { user: userId, event: eventId },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  return {
    getMyEvents,
    subscribeParticipant,
    unSubscribeParticipant,
  };
};

//                      SURVEY & SURVEYlINE
const useSurveys = () => {
  const [token, setToken] = useState("");

  getItem(TOKEN_NAME)
    .then((res) => {
      setToken(res);
    })
    .catch((e) => {
      console.log("token error custom hooks : ", e);
    });

  const getSurveys = async (eventId, userId) => {
    return await axios({
      method: "get",
      url: `${REACT_APP_API_URL}/survey/${eventId}/${userId}`,
    });
  };

  const createSurvey = async ({ event, title, description }) => {
    return await axios({
      method: "post",
      url: `${REACT_APP_API_URL}/survey/`,
      data: {
        event,
        label: title,
        description,
        role: "organizer",
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  const deleteSurvey = async ({ id }) => {
    return await axios({
      method: "delete",
      url: `${REACT_APP_API_URL}/survey/`,
      data: {
        id,
        role: "organizer",
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  const updateSurvey = async ({ id, title, description }) => {
    return await axios({
      method: "patch",
      url: `${REACT_APP_API_URL}/survey/`,
      data: {
        id,
        label: title,
        description,
        role: "organizer",
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  return {
    getSurveys,
    createSurvey,
    deleteSurvey,
    updateSurvey,
  };
};

const useSurveyLine = () => {
  const [token, setToken] = useState("");

  getItem(TOKEN_NAME)
    .then((res) => {
      setToken(res);
    })
    .catch((e) => {
      console.log("token error custom hooks : ", e);
    });

  const updateSurveyLine = async ({
    label,
    nb_votes,
    survey,
    line_number,
    liked,
  }) => {
    return await axios({
      method: "patch",
      url: `${REACT_APP_API_URL}/surveyLine/`,
      data: {
        label,
        survey,
        liked,
        nbVotes: nb_votes,
        lineNumber: line_number,
        role: "organizer",
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  const createSurveyLine = async ({ label, survey }) => {
    return await axios({
      method: "post",
      url: `${REACT_APP_API_URL}/surveyLine/`,
      data: {
        label,
        survey,
        role: "organizer",
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  const deleteSurveyLine = async ({ line_number, survey }) => {
    return await axios({
      method: "delete",
      url: `${REACT_APP_API_URL}/surveyLine/`,
      data: {
        lineNumber: line_number,
        survey,
        role: "organizer",
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  return {
    updateSurveyLine,
    createSurveyLine,
    deleteSurveyLine,
  };
};

const useLike = () => {
  const [token, setToken] = useState("");

  getItem(TOKEN_NAME)
    .then((res) => {
      setToken(res);
    })
    .catch((e) => {
      console.log("token error custom hooks : ", e);
    });

  const like = async ({ survey_line, person }) => {
    return await axios({
      method: "post",
      url: `${REACT_APP_API_URL}/like/`,
      data: {
        survey_line,
        person,
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  const unLike = async ({ survey_line, person }) => {
    return await axios({
      method: "delete",
      url: `${REACT_APP_API_URL}/like/`,
      data: {
        survey_line,
        person,
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  return {
    like,
    unLike,
  };
};

//              INVITATION

const useInvitation = () => {
  const [token, setToken] = useState("");

  getItem(TOKEN_NAME)
    .then((res) => {
      setToken(res);
    })
    .catch((e) => {
      console.log("token error custom hooks : ", e);
    });

  const getInvitations = async ({ email }) => {
    return await axios({
      method: "get",
      url: `${REACT_APP_API_URL}/invitation/${email}`,
    });
  };

  const getInvitationsByEventId = async ({eventId}) => {
    return await axios({
      method: "get",
      url: `${REACT_APP_API_URL}/invitation/event/${eventId}`,
    });
  }

  const createInvitation = async ({ userId, eventId, guest }) => {
    return await axios({
      method: "post",
      url: `${REACT_APP_API_URL}/invitation/`,
      data: {
        userId,
        eventId,
        guest,
        role: "organizer",
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  const deleteInvitation = async ({ guest, eventId }) => {
    return await axios({
      method: "delete",
      url: `${REACT_APP_API_URL}/invitation/`,
      data: {
        guest,
        eventId,
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  const deleteInvitationByEvent = async ({ eventId }) => {
    return await axios({
      method: "delete",
      url: `${REACT_APP_API_URL}/invitation/${eventId}`,
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  const updateInvitation = async ({ eventId, guest, accepted }) => {
    return await axios({
      method: "patch",
      url: `${REACT_APP_API_URL}/invitation/`,
      data: {
        guest,
        eventId,
        accepted,
      },
      headers: {
        Authorization: "Bearer " + token,
      },
    });
  };

  return {
    getInvitations,
    deleteInvitation,
    createInvitation,
    updateInvitation,
    deleteInvitationByEvent,
    getInvitationsByEventId,
  };
};

export {
  useEvent,
  useEventRole,
  useUser,
  useSurveys,
  useSurveyLine,
  useLike,
  useInvitation,
};
