export const wait = (timeOut = 4000) => {
  return new Promise((resolve) => setTimeout(resolve, timeOut));
};

export const dateDiff = (date1, date2) => {
  const diff = {};
  let tmp = date2 - date1;

  tmp = Math.floor(tmp / 1000);
  diff.sec = tmp % 60;

  tmp = Math.floor((tmp - diff.sec) / 60);
  diff.min = tmp % 60;

  tmp = Math.floor((tmp - diff.min) / 60);
  diff.hour = tmp % 24;

  tmp = Math.floor((tmp - diff.hour) / 24);
  diff.day = tmp;

  diff.sec < 0 && (diff.sec = 0);

  return diff;
};

export const creationTime = ({ day, hour, min, sec }) => {
  let message;
  let temp;

  if (day > 0) {
    if (day >= 365) {
      temp = Math.floor(day / 365);
      message = `il y'a ${temp} an${temp > 1 ? "s" : ""}`;
    } else if (day >= 30) {
      temp = Math.floor(day / 30);
      message = `il y'a ${temp} mois`;
    } else if (day >= 7) {
      temp = Math.floor(day / 7);
      message = `il y'a ${temp} semaine${temp > 1 ? "s" : ""}`;
    } else {
      message = `il y'a ${day} jour${day > 1 ? "s" : ""}`;
    }
  } else if (hour > 0) {
    message = `il y'a ${hour} heure${hour > 1 ? "s" : ""}`;
  } else if (min > 0) {
    message = `il y'a ${min} minute${min > 1 ? "s" : ""}`;
  } else if (sec >= 0) {
    message = `à l'instant`;
  }

  return message;
};

export const dateToString = (date) => {
  const myDate = {
    day: date.getDate(),
    month: date.getMonth() + 1,
    year: date.getFullYear(),
  };
  return `${myDate.day < 10 ? "0" : ""}${myDate.day}/${
    myDate.month < 10 ? "0" : ""
  }${myDate.month}/${myDate.year}`;
  //`${myDate.day < 10 ? '0' : ''}${myDate.day}/${myDate.month < 10 ? '0' : ''}${myDate.month}/${myDate.year}`
};

export const eventTime = ({ day }) => {
  let message;
  let temp;

  if (day >= 365) {
    temp = day / 365;
    message = `${temp} an${temp > 1 ? "s" : ""}`;
  } else if (day >= 30) {
    temp = day / 30;
    message = `${temp} mois`;
  } else if (day >= 7) {
    temp = day / 7;
    message = `${temp} semaine${temp > 1 ? "s" : ""}`;
  } else {
    message = `${day} jour${day > 1 ? "s" : ""}`;
  }
  return message;
};

export const findUsers = ({ searchWord, users }) => {
  return users.where(
    (u) =>
      u.email?.contains(searchWord) ||
      u.last_name?.contains(searchWord) ||
      u.first_name?.contains(searchWord)
  );
};

export const alreadyParticipate = ({ invites, guest, event }) => {
  let isInvited = false;

  let i = 0;
  while (i < invites.length && !isInvited) {
    // isInvited =
    //   event.participants?.find((p) => p.email === guest) !== undefined;

    //   console.log("participe : ", event.participants?.find((p) => p.email === guest));

   // if (!isInvited) {
      const invitation =
        invites[i].invitations.find((inv) => inv.event.id === event.id);
        isInvited = invitation !== undefined;
       
   // }
    i++;
  }

  return isInvited;
};
