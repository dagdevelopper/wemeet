
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import { store } from './src/redux_wemeet/Store';
import { LogBox } from 'react-native';
import Root from './src/navigation/root';
import { useState } from 'react';
import Auth from './src/auth/authProvider/index';

LogBox.ignoreLogs(["EventEmitter.removeListener"]);



export default function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [userConnected, setUserConnected] = useState({});
  const [nbEventsOrganized, setOrganized] = useState(0);
  const [nbEventsParticipated, setParticipated] = useState(0);


  return (
    <Provider store={store}>
      <Auth.Provider value={{ isAuthenticated, setIsAuthenticated, userConnected, setUserConnected, nbEventsOrganized, nbEventsParticipated, setOrganized, setParticipated }}>
        <SafeAreaProvider>
          <StatusBar style='auto' />
          <NavigationContainer>
            <Root />
          </NavigationContainer>
        </SafeAreaProvider>
      </Auth.Provider>
    </Provider>
  );
}


