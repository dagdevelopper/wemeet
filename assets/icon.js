import SearchIcon from '../assets/search.png';
import AddIcon from '../assets/add.png';
import PersonIcon from '../assets/account.png';
import EventIcon from '../assets/event.png';
import LocationIcon from '../assets/location.png';
import LogoIcon from '../assets/logo2.jpg';
import MailIcon from '../assets/mail.png';
import PasswordIcon from '../assets/password.png';
import SurveyIcon from '../assets/survey.png';
import CloseIcon from '../assets/close.png';
import EventUnknown from '../assets/event_unknown.png';
import MenuIcon from '../assets/menu.png';
import WemeetIcon from '../assets/wemeet.png';
import ProfileIcon from '../assets/random.jpg';
import NextIcon from '../assets/next.png';
import EditIcon from '../assets/edit.png';
import HomeIcon from '../assets/home.png';
import LogoutIcon from '../assets/logout.png';
import DoneIcon from '../assets/done.png';
import VisibleIcon from '../assets/visible.png';
import UnvisibleIcon from '../assets/unvisible.png';
import WifiOffIcon from '../assets/wifiUnable.png';
import VerifiedBadgeIcon from '../assets/badgeVerified.png';
import PublicIcon from '../assets/public.png';
import PrivateIcon from '../assets/private.png';
import DeleteIcon from '../assets/delete.png';
import LikeIcon from '../assets/like.png';
import DislikeIcon from '../assets/dislike.png';
import PlusIcon from '../assets/plus.png';
import MoreIcon from '../assets/more.png';
import ScheduleIcon from '../assets/schedule.png';
import WarningIcon from '../assets/warning.png';
import ErrorIcon from '../assets/error.png';
import InvitIcon from '../assets/invitation.png';





export {
    SearchIcon, AddIcon, PersonIcon,
    EventIcon, LocationIcon, LogoIcon, MailIcon,
    PasswordIcon, SurveyIcon, CloseIcon, EventUnknown, MenuIcon,
    WemeetIcon, ProfileIcon, NextIcon, EditIcon,
    HomeIcon, LogoutIcon, DoneIcon, VisibleIcon, UnvisibleIcon, WifiOffIcon,
    VerifiedBadgeIcon, PublicIcon, PrivateIcon, DeleteIcon, LikeIcon, DislikeIcon,
    PlusIcon, MoreIcon, ScheduleIcon, WarningIcon, ErrorIcon, InvitIcon
}
